﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Engine.Graphics._3D.Loaders;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;

namespace Engine.Graphics._3D
{
	public class Mesh
	{
		public delegate bool RaycastFilter(vec3[] triangle, vec3 p, vec3 n, float t);

		public static Dictionary<string, Mesh> Load(string filename)
		{
			Debug.Assert(File.Exists(Paths.Meshes + filename), $"Missing mesh '{filename}'.");

			if (filename.EndsWith(".obj"))
			{
				return ObjLoader.Load(filename);
			}

			var mesh = DaeLoader.Load(filename);

			return new Dictionary<string, Mesh>
			{
				{"", mesh}
			};
		}

		public Mesh(vec3[] points, vec2[] source, vec3[] normals, ivec3[] vertices, ushort[] indices, string texture,
			ivec2[] boneIndexes = null, vec2[] boneWeights = null)
		{
			float minX = points.Min(p => p.x);
			float maxX = points.Max(p => p.x);
			float minY = points.Min(p => p.y);
			float maxY = points.Max(p => p.y);
			float minZ = points.Min(p => p.z);
			float maxZ = points.Max(p => p.z);
			float width = maxX - minX;
			float height = maxY - minY;
			float depth = maxZ - minZ;

			Points = points;
			Source = source;
			Normals = normals;
			Vertices = vertices;
			Indices = indices;
			MaxIndex = indices.Max();

			// TODO: Once meshes are finalized, bounds could be pre-computed (if bounds are still needed at all).
			Bounds = new vec3(width, height, depth);
			Origin = new vec3(minX, minY, minZ);
			BoneIndexes = boneIndexes;
			BoneWeights = boneWeights;
			Texture = ContentCache.GetTexture(texture ?? "Grey.png");
		}

		public vec3[] Points { get; }
		public vec2[] Source { get; }
		public vec3[] Normals { get; }

		// Each vertex is ordered position (X), source (Y), and normal (Z).
		public ivec3[] Vertices { get; }

		// These fields help when creating shapes around meshes (such as physics bodies or sensors).
		public vec3 Bounds { get; }
		public vec3 Origin { get; }

		// These two arrays will be left null for non-animated meshes. Bone data can optionally be set programatically
		// (rather than loaded from a file).
		public ivec2[] BoneIndexes { get; set; }
		public vec2[] BoneWeights { get; set; }

		public ushort[] Indices { get; }
		public ushort MaxIndex { get; }

		public Texture Texture { get; }
		public MeshHandle Handle { get; set; }

		public bool Raycast(vec3 start, vec3 direction, float range, out RaycastResults results)
		{
			return Raycast(start, direction, range, out results, null, false);
		}

		public bool Raycast(vec3 start, vec3 direction, float range, out RaycastResults results, RaycastFilter filter,
			bool shouldNormalizeInFilter, float expansion = 0)
		{
			var tMin = -1f;

			vec3 p;
			vec3 n;
			vec3[] triangle = null;

			var indexes = ivec3.Zero;

			for (int i = 0; i < Indices.Length; i += 3)
			{
				var i0 = Vertices[Indices[i]].x;
				var i1 = Vertices[Indices[i + 1]].x;
				var i2 = Vertices[Indices[i + 2]].x;
				var tri = new []
				{
					Points[i0],
					Points[i1],
					Points[i2]
				};

				// Using a very small expansion amount can help catch potential floating-point errors when a raycast
				// would exactly hit the edge of another triangle. Useful for some algorithms.
				if (expansion > 0)
				{
					tri = Utilities.Expand(tri, expansion);
				}

				if (Utilities.Intersects(start, start + direction * range, tri, out var t) && (tMin < 0 || t < tMin))
				{
					bool shouldPopulate;

					if (filter == null)
					{
						shouldPopulate = true;
					}
					else
					{
						p = start + direction * range * t;
						n = Utilities.ComputeNormal(tri, WindingTypes.Clockwise, shouldNormalizeInFilter);
						shouldPopulate = filter(tri, p, n, t);
					}

					if (shouldPopulate)
					{
						tMin = t;
						triangle = tri;
						indexes = new ivec3(i0, i1, i2);
					}
				}
			}

			if (tMin < 0)
			{
				results = null;

				return false;
			}

			p = start + direction * range * tMin;
			n = Utilities.ComputeNormal(triangle, WindingTypes.Clockwise);
			results = new RaycastResults(p, n, triangle, indexes, tMin);

			return true;
		}
	}
}
