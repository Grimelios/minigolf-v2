﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Interfaces._2D;
using Engine.Utility;
using GlmSharp;

namespace Engine.Interpolation._2D
{
	public class RotationInterpolator : Interpolator<float>
	{
		private IRotatable target;
		private IRenderRotatable renderTarget;
		private IRotationContainer containerTarget;

		public RotationInterpolator(IRotatable target, EaseTypes easeType) :
			this(target, 0, 0, easeType)
		{
		}

		public RotationInterpolator(IRenderRotatable target, EaseTypes easeType) :
			this(target, 0, 0, easeType)
		{
		}

		public RotationInterpolator(IRotationContainer target, EaseTypes easeType) :
			this(target, 0, 0, easeType)
		{
		}

		public RotationInterpolator(IRotatable target, float start, float end, EaseTypes easeType,
			float duration = 0) : base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			this.target = target;
		}

		public RotationInterpolator(IRenderRotatable target, float start, float end, EaseTypes easeType,
			float duration = 0) : base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			renderTarget = target;
		}

		public RotationInterpolator(IRotationContainer target, float start, float end, EaseTypes easeType,
			float duration = 0) : base(start, end, duration, easeType)
		{
			Debug.Assert(target != null, NullMessage);

			containerTarget = target;
		}

		protected override void Lerp(float t)
		{
			var p = Utilities.Lerp(Start, End, t);

			if (target != null)
			{
				target.Rotation = p;
			}
			else if (renderTarget != null)
			{
				renderTarget.Rotation.SetValue(p, true);
			}
			else
			{
				containerTarget.SetRotation(p, true);
			}
		}
	}
}
