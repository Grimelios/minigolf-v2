﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Engine.Core._3D;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;

namespace Engine.Entities
{
	public class SceneFragment
	{
		private static uint environmentGroup_;
		private static uint fineEnvironmentGroup_;
		private static uint coarseEnvironmentGroup_;

		// This solution feels a bit hacky, but it allows all physics groups to be contained in a single class in the
		// game project.
		public static void Initialize(uint environmentGroup, uint fineEnvironmentGroup, uint coarseEnvironmentGroup)
		{
			environmentGroup_ = environmentGroup;
			fineEnvironmentGroup_ = fineEnvironmentGroup;
			coarseEnvironmentGroup_ = coarseEnvironmentGroup;
		}

		public static SceneFragment Load(string filename, Scene scene)
		{
			Debug.Assert(environmentGroup_ > 0, "Environment physics groups must be initialized.");

			var json = JsonUtilities.Load("Fragments/" + filename);
			var jStatic = json["Static"];

			// All fragments are required to specify a static mesh.
			Debug.Assert(jStatic != null, $"Fragment {filename} missing static block.");

			var jMap = jStatic["Map"];
			var jPhysics = jStatic["Physics"];
			var jOrigin = json["Origin"];
			var jSpawn = json["Spawn"];

			Debug.Assert(jMap != null, $"Fragment {filename} missing static map.");
			Debug.Assert(jOrigin != null, $"Fragment {filename} missing origin.");
			Debug.Assert(jSpawn != null, $"Fragment {filename} missing player spawn point.");

			var origin = Utilities.ParseVec3(jOrigin.Value<string>());
			var model = new Model(jMap.Value<string>());
			
			model.Position.SetValue(origin, false);

			RigidBody body = null;

			// In rare cases, it's valid to omit the static physics mesh (for example, when loading a fragment in order
			// to record footage without necessarily controlling the player).
			if (jPhysics != null)
			{
				var shape = TriangleMeshLoader.Load(jPhysics.Value<string>());

				// TODO: Use the fine or coarse group as appropriate.
				body = new RigidBody(shape, environmentGroup_, RigidBodyTypes.Static);
				body.Position = origin.ToJVector();

				var material = body.Material;
				material.KineticFriction = 0;
				material.StaticFriction = 0;
			}

			var fragment = new SceneFragment(filename);
			fragment.Entities = LoadEntities(json, scene, origin);
			fragment.MapModel = model;
			fragment.MapBody = body;
			fragment.Origin = origin;
			fragment.Spawn = Utilities.ParseVec3(jSpawn.Value<string>());

			return fragment;
		}

		private static Entity[] LoadEntities(JObject json, Scene scene, vec3 origin)
		{
			var array = (JArray)json["Entities"];

			// It's valid for a scene to contain no entities (although unlikely in the finished game).
			if (array == null)
			{
				return null;
			}

			var entities = new Entity[array.Count];

			for (int i = 0; i < array.Count; i++)
			{
				var block = array[i];
				var path = $"{Game.Assembly}.Entities.{block["Type"].Value<string>()}, {Game.Assembly}";
				var type = Type.GetType(path);

				Debug.Assert(type != null, $"Missing entity type {path}.");

				// Position will almost always be given, but can be omitted for certain entities (like rope bridges,
				// which instead specify endpoints).
				var jPosition = block["Position"];
				var jOrientation = block["Orientation"];
				var p = jPosition != null ? Utilities.ParseVec3(jPosition.Value<string>()) : vec3.Zero;
				var q = jOrientation != null ? ParseQuat(jOrientation) : quat.Identity;
				var entity = (Entity)Activator.CreateInstance(type);

				// The entity's transform (position and orientation) is intentionally set before initialization (useful
				// for a few purposes, like adding motors).
				entity.SetPosition(origin + p, false);
				entity.SetOrientation(q, false);
				entity.Initialize(scene, block);
				entities[i] = entity;
			}

			// This is used for the debug assertion below.
			var ids = new HashSet<int>();

			foreach (var entity in entities)
			{
				var id = entity.Id;

				if (id == -1)
				{
					continue;
				}

				if (ids.Contains(id))
				{
					Debug.Fail($"Duplicate entity ID ({id}).");
				}

				ids.Add(id);
			}

			return entities;
		}

		private static quat ParseQuat(JToken token)
		{
			// TODO: Should probably use radians directly at some point.
			// Orientation is given as Euler angles (in degrees, not radians).
			var tokens = token.Value<string>().Split('|');

			Debug.Assert(tokens.Length == 3, "Entity orientation has the wrong format (should be pipe-separated " +
				"Euler angles, given in degrees).");

			float x = Utilities.ToRadians(float.Parse(tokens[0]));
			float y = Utilities.ToRadians(float.Parse(tokens[1]));
			float z = Utilities.ToRadians(float.Parse(tokens[2]));

			// TODO: This might be wrong for non-zero angles on multiple axes. Should be investigated.
			return quat.FromAxisAngle(x, vec3.UnitX) *
				quat.FromAxisAngle(y, vec3.UnitY) *
				quat.FromAxisAngle(z, vec3.UnitZ);
		}

		private SceneFragment(string filename)
		{
			Filename = filename;
		}

		// This is used for asserting that the fragment aren't loaded multiple times.
		public string Filename { get; }

		public Entity[] Entities { get; private set; }
		public Model MapModel { get; private set; }
		public RigidBody MapBody { get; private set; }

		// This is the default player spawn within the fragment. Only applicable if loading into that fragment
		// directly (used frequently during development).
		public vec3 Spawn { get; private set; }
		public vec3 Origin { get; private set; }
	}
}
