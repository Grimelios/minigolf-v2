﻿using System.Collections.Generic;
using Engine.Input.Data;
using Engine.Interfaces._2D;
using Engine.Utility;
using static Engine.GLFW;

namespace Engine.Input
{
	// TODO: If menu items can move, might need special handling if that item moves over a stationary cursor.
	public class ClickableSet<T> where T : class, IClickable
	{
		private T hoveredItem;
		private IEnumerable<T> items;

		public ClickableSet(IEnumerable<T> items)
		{
			this.items = items;
		}

		// The hovered item can be overidden externally under some circumstances (for example, a hovered item becoming
		// disabled).
		public T HoveredItem
		{
			get => hoveredItem;
			set => hoveredItem = value;
		}

		// Mouse submissions (i.e. clicking an item) takes priority over submission via other input methods (like
		// pressing enter on a keyboard).
		public ClickFlags ProcessMouse(MouseData data)
		{
			var location = data.Location;
			var flags = ClickFlags.None;

			if (hoveredItem != null && !hoveredItem.Contains(location))
			{
				hoveredItem.OnUnhover();
				hoveredItem = null;
			}

			foreach (T item in items)
			{
				if (item.IsEnabled && item != hoveredItem && item.Contains(location))
				{
					// It's possible for the mouse to move between items in a single frame.
					hoveredItem?.OnUnhover();
					hoveredItem = item;
					hoveredItem.OnHover(location);

					break;
				}
			}

			if (hoveredItem == null)
			{
				return flags;
			}

			// Hovering only counts if the mouse also moved this frame (useful for prioritizing conflicting selection
			// changes from different devices, e.g. between keyboard and mouse).
			if (Utilities.LengthSquared(data.Delta) > 0)
			{
				flags |= ClickFlags.WasHovered;
			}

			// TODO: Probably allow right click selection? (for accessibility)
			// It's also possible for the mouse to move to a new item and click on the same frame (should be rare, but
			// would be considered a valid click for something like a TAS).
			if (data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates.PressedThisFrame))
			{
				// TODO: Select the item if currently unselected.
				hoveredItem.OnClick(location);
				flags |= ClickFlags.WasClicked;
			}

			return flags;
		}
	}
}
