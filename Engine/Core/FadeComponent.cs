﻿using System.Diagnostics;
using Engine.Interfaces;
using Engine.Timing;

namespace Engine.Core
{
	public class FadeComponent : IComponent
	{
		private SingleTimer timer;

		private float fadeTime;
		private float holdTime;

		// Zero means fading in, one means holding, and two means fading out.
		private int state;

		public FadeComponent(float fadeTime, float holdTime)
		{
			Debug.Assert(fadeTime > 0, "Fade time must be positive (when creating a fade component).");
			Debug.Assert(holdTime > 0, "Hold time must be positive (when creating a fade component).");

			this.fadeTime = fadeTime;
			this.holdTime = holdTime;

			timer = new SingleTimer(t =>
			{
				if (++state == 3)
				{
					Reset();
				}
				else if (state == 1)
				{
					timer.Duration = holdTime;
					timer.Tick = null;
				}
				else
				{
					timer.Duration = fadeTime;
					timer.Tick = Fade;
				}
			}, fadeTime, TimerFlags.IsPaused | TimerFlags.IsRepeatable);

			timer.Tick = Fade;
			Color = Color.Transparent;
		}

		public bool IsComplete => false;
		public bool IsEnabled => !timer.IsPaused;

		public Color Color { get; private set; }

		private void Fade(float t)
		{
			if (state == 2)
			{
				t = 1 - t;
			}

			// TODO: Consider adding a target color parameter to the constructor.
			Color = Color.Lerp(Color.Transparent, Color.White, t);
		}

		public void Start()
		{
			timer.IsPaused = false;
		}

		public void Reset()
		{
			state = 0;
			Color = Color.Transparent;

			timer.Reset();
			timer.Duration = fadeTime;
			timer.Tick = Fade;
		}

		public void Update()
		{
			timer.Update();
		}
	}
}
