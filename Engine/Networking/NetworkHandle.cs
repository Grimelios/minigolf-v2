﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Networking
{
	public class NetworkHandle
	{
		public NetworkHandle(uint packetType, int index)
		{
			PacketType = packetType;
			Index = index;
		}

		public uint PacketType { get; }
		public int Index { get; }
	}
}
