﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Lidgren.Network;

namespace Engine.Networking
{
	public abstract class GamePeer
	{
		private NetPeer peer;
		private Dictionary<uint, List<INetworked>> targets;

		protected GamePeer(int port)
		{
			var config = new NetPeerConfiguration("Minigolf");
			config.AcceptIncomingConnections = true;
			config.Port = port;
			config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

			peer = new NetPeer(config);
			targets = new Dictionary<uint, List<INetworked>>();
		}

		public int Port => peer.Configuration.Port;
		public int Connections => peer.Connections.Count;

		public void Start()
		{
			Debug.Assert(peer.Status == NetPeerStatus.NotRunning, "Peer was already started.");

			peer.Start();
		}

		protected void Connect(string host, int port, Packet hailPacket)
		{
			Debug.Assert(!string.IsNullOrEmpty(host), "Host can't be null or empty.");
			Debug.Assert(port > 0 && port.ToString().Length == 5, "Port must be a positive 5-digit integer.");

			var hail = CreateMessage(hailPacket, true);
			peer.Connect(host, port, hail);
		}

		protected void ForwardToTargets(Packet packet)
		{
			if (targets.TryGetValue(packet.PacketType, out var list))
			{
				list.ForEach(t => t.Process(packet));
			}
		}

		public void Send(Packet packet, NetDeliveryMethod method)
		{
			Send(packet, null, null, method);
		}

		public void Send(Packet packet, NetDeliveryMethod method, NetConnection excluded)
		{
			Send(packet, null, excluded, method);
		}

		public void Send(Packet packet, NetConnection recipient, NetDeliveryMethod method)
		{
			Send(packet, recipient, null, method);
		}

		private void Send(Packet packet, NetConnection recipient, NetConnection excluded, NetDeliveryMethod method)
		{
			var connections = peer.Connections;

			if (connections.Count == 0)
			{
				return;
			}

			Debug.Assert(packet != null, "Can't send a null packet.");

			var message = CreateMessage(packet);

			// Recipient and excluded connections are mutually exclusive.
			if (recipient != null)
			{
				peer.SendMessage(message, recipient, method);
			}
			else if (excluded != null)
			{
				peer.SendMessage(message, connections.Where(c => c != excluded).ToList(), method, 0);
			}
			else
			{
				peer.SendMessage(message, peer.Connections, NetDeliveryMethod.Unreliable, 0);
			}
		}

		protected NetOutgoingMessage CreateMessage(Packet packet, bool suppressType = false)
		{
			var message = peer.CreateMessage();

			// Suppressing the type can be useful when the packet type will be implied on the other side (such as
			if (!suppressType)
			{
				message.Write((byte)packet.PacketType);
			}

			packet.Encode(message);

			return message;
		}

		public void Subscribe(uint packetType, INetworked target)
		{
			Debug.Assert(target != null, "Can't subscribe a null network target.");

			if (!targets.TryGetValue(packetType, out var list))
			{
				list = new List<INetworked>();
				targets.Add(packetType, list);
			}

			Debug.Assert(!list.Contains(target), $"Duplicate network target (for packet type {packetType}).");

			// New targets are always added to the first available slot.
			var index = 0;

			while (index < list.Count && list[index] != null)
			{
				index++;
			}

			var handles = target.NetworkHandles;

			if (handles == null)
			{
				handles = new List<NetworkHandle>();
				target.NetworkHandles = handles;
			}

			handles.Add(new NetworkHandle(packetType, index));
			list.Add(target);
		}

		public void Unsubscribe(INetworked target, uint? packetType = null)
		{
			Debug.Assert(target != null, "Can't unsubscribe a null network target.");

			// -1 indicates that the target should be unsubscribed from all packet types.
			if (!packetType.HasValue)
			{
				target.NetworkHandles.ForEach(h => targets[h.PacketType].RemoveAt(h.Index));
			}
			else
			{
				var handle = target.NetworkHandles.First(h => h.PacketType == packetType);
				targets[packetType.Value].RemoveAt(handle.Index);
			}
		}

		public void Refresh()
		{
			NetIncomingMessage message;

			while ((message = peer.ReadMessage()) != null)
			{
				switch (message.MessageType)
				{
					case NetIncomingMessageType.Data:
						var packetType = (int)message.ReadByte();
						var packet = CreateDataPacket(packetType);

						packet.Decode(message);
						packet.Connection = message.SenderConnection;

						ForwardToTargets(packet);

						break;

					case NetIncomingMessageType.DebugMessage:
						break;

					case NetIncomingMessageType.StatusChanged:
						OnStatusChange(message);

						break;
				}

				peer.Recycle(message);
			}
		}

		protected abstract Packet CreateDataPacket(int packetType);
		protected abstract void OnStatusChange(NetIncomingMessage message);
	}
}
