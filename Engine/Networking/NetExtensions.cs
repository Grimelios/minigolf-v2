﻿using GlmSharp;
using Lidgren.Network;

namespace Engine.Networking
{
	public static class NetExtensions
	{
		public static vec3 ReadVec3(this NetIncomingMessage message)
		{
			var x = message.ReadFloat();
			var y = message.ReadFloat();
			var z = message.ReadFloat();

			return new vec3(x, y, z);
		}

		public static quat ReadQuat(this NetIncomingMessage message)
		{
			var x = message.ReadFloat();
			var y = message.ReadFloat();
			var z = message.ReadFloat();
			var w = message.ReadFloat();

			return new quat(x, y, z, w);
		}

		public static void Write(this NetOutgoingMessage message, vec3 value)
		{
			message.Write(value.x);
			message.Write(value.y);
			message.Write(value.z);
		}

		public static void Write(this NetOutgoingMessage message, quat value)
		{
			message.Write(value.x);
			message.Write(value.y);
			message.Write(value.z);
			message.Write(value.w);
		}
	}
}
