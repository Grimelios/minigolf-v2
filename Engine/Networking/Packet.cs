﻿using Lidgren.Network;

namespace Engine.Networking
{
	public abstract class Packet
	{
		protected Packet(uint type)
		{
			PacketType = type;
		}

		public uint PacketType { get; }

		// This is populated when an incoming data packet is received.
		public NetConnection Connection { get; set; }

		public abstract void Decode(NetIncomingMessage message);
		public abstract void Encode(NetOutgoingMessage message);
	}
}
