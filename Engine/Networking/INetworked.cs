﻿using System;
using System.Collections.Generic;

namespace Engine.Networking
{
	public interface INetworked : IDisposable
	{
		List<NetworkHandle> NetworkHandles { get; set; }

		void Process(Packet packet);
	}
}
