﻿using System;
using System.Collections.Generic;
using Engine.Graphics._2D;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.UI;
using Engine.View;
using Minigolf.Networking;
using Minigolf.Settings;
using Minigolf.UI.Menus;

namespace Minigolf.Loops
{
	public abstract class GameLoop : IDynamic, IRenderTargetUser3D
	{
		protected List<IRenderTargetUser3D> renderTargetUsers3D;

		protected GameLoop(LoopTypes type)
		{
			LoopType = type;
			renderTargetUsers3D = new List<IRenderTargetUser3D>();
		}

		public LoopTypes LoopType { get; }
		
		public Camera3D Camera { get; set; }
		public Canvas Canvas { get; set; }
		public MasterSettings Settings { get; set; }
		public MenuManager MenuManager { get; set; }
		public MinigolfPeer Peer { get; set; }
		public SpriteBatch SpriteBatch { get; set; }

		public virtual void Dispose()
		{
		}

		public abstract void Initialize(MainGame game);

		public virtual void Update()
		{
		}

		public void DrawTargets(float t)
		{
			renderTargetUsers3D.ForEach(r => r.DrawTargets(t));
		}

		public virtual void Draw(float t)
		{
		}
	}
}
