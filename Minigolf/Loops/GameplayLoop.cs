﻿using System.Linq;
using Engine;
using Engine.Core;
using Engine.Editing;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Physics;
using Engine.Sensors;
using Engine.UI;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Jitter;
using Jitter.Collision;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Minigolf.Data;
using Minigolf.Entities;
using Minigolf.Jelly;
using Minigolf.Players;
using Minigolf.UI.Hud;
using Minigolf.UI.Menus;
using Minigolf.View;
using static Engine.GLFW;

namespace Minigolf.Loops
{
	public class GameplayLoop : GameLoop
	{
		private Course course;
		private Round round;
		private Putter putter;
		private Scene scene;
		private Space space;
		private World world;
		private PauseMenu pauseMenu;
		private FollowSnapper snapper;
		private PlayerCoordinator coordinator;

		//private JellyMesh jellyMesh;

		private string playerName;

		private JitterVisualizer jitterVisualizer;
		private SpaceVisualizer spaceVisualizer;

		public GameplayLoop(string playerName) : base(LoopTypes.Gameplay)
		{
			this.playerName = playerName;
		}
		
		public override void Initialize(MainGame game)
		{
			var system = new CollisionSystemSAP();
			system.UseTriangleMeshNormal = true;
			
			// TODO: Use a property for gravity.
			world = new World(system);
			world.Gravity = new JVector(0, -20, 0);
			world.Events.ContactCreated += OnContact;

			space = new Space();
			scene = new Scene
			{
				Camera = Camera,
				Canvas = Canvas,
				Peer = Peer,
				Space = space,
				World = world
			};

			//jellyMesh = new JellyMesh("Jelly.obj", scene);
			//jellyMesh = new JellyMesh("JellySimple.obj", scene);
			//jellyMesh = new JellyMesh("WindmillCourse.obj", scene);

			jitterVisualizer = new JitterVisualizer(Camera, world);
			//jitterVisualizer.IsEnabled = true;
			spaceVisualizer = new SpaceVisualizer(Camera, space);
			//spaceVisualizer.IsEnabled = true;

			var light = scene.Renderer.Light;
			light.Direction = Utilities.Normalize(new vec3(1, -0.2f, 0.5f));

			Canvas.Load("Hud.json");

			var terminal = Canvas.GetElement<Terminal>();
			terminal.Add(new FreecamCommand(Camera, 25));

			var followView = new FollowView(Settings.Control);

			Camera.Attach(followView);

			course = new Course(scene);

			var holes = course.Load("WindmillCourse.json");

			round = new Round(Peer, -1, -1);
			putter = new Putter(Peer, Camera, round, Canvas.GetElement<StrokeDisplay>());
			snapper = new FollowSnapper(followView);

			var renderer = scene.Renderer;
			var courseLighting = course.Lighting;
			var sceneLighting = renderer.Light;

			sceneLighting.Color.SetValue(courseLighting.Color, false);
			sceneLighting.Direction = courseLighting.Direction;
			sceneLighting.AmbientIntensity = courseLighting.Ambient;

			renderTargetUsers3D.Add(renderer);

			// TODO: Pull local name from another source.
			coordinator = new PlayerCoordinator(scene, Peer, course, round, putter);
			coordinator.AddPlayer(playerName);
			coordinator.IsLeader = Peer.Port == 53701;

			round.Start(holes);

			// TODO: Probably don't hardcode position.
			pauseMenu = new PauseMenu(game);
			pauseMenu.SetPosition(new vec2(Resolution.WindowWidth / 2f, 80), false);

			MenuManager.Add(pauseMenu);
			game.PauseElements.Add(pauseMenu);

			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				/*
				if (keyboard.Query(GLFW_KEY_R, InputStates.PressedThisFrame))
				{
					golfball.Reset();
				}
				*/

				if (!keyboard.Query(InputStates.Held, GLFW_KEY_LEFT_ALT, GLFW_KEY_RIGHT_ALT))
				{
					return;
				}

				if (keyboard.Query(GLFW_KEY_J, InputStates.PressedThisFrame))
				{
					jitterVisualizer.IsEnabled = !jitterVisualizer.IsEnabled;
				}

				if (keyboard.Query(GLFW_KEY_S, InputStates.PressedThisFrame))
				{
					spaceVisualizer.IsEnabled = !spaceVisualizer.IsEnabled;
				}

				if (keyboard.Query(GLFW_KEY_M, InputStates.PressedThisFrame))
				{
					//renderer.IsEnabled = !renderer.IsEnabled;
				}

				if (keyboard.Query(GLFW_KEY_C, InputStates.PressedThisFrame) && !coordinator.IsLeader)
				{
					Peer.Connect("localhost", 53701, coordinator.LocalPlayers[0].Id);
				}
			});
		}

		private bool OnContact(RigidBody body1, RigidBody body2, JVector point1, JVector point2, JVector normal,
			JVector[] triangle, float penetration)
		{
			// By design, all physics objects have entities attached, with the exception of static parts of the map. In
			// the case of map collisions, it's unknown which body comes first as an argument (as of writing this
			// comment, anyway), which is why both entities are checked for null.
			var entity1 = body1.Tag as Entity;
			var entity2 = body2.Tag as Entity;
			var p1 = point1.ToVec3();
			var p2 = point2.ToVec3();

			// The normal needs to be flipped based on how Jitter handles triangle winding.
			var n = Utilities.Normalize(normal.ToVec3());

			// A triangle will only be given in the case of collisions with a triangle mesh (or terrain).
			if (triangle != null)
			{
				var entity = entity1 ?? entity2;
				var point = entity1 != null ? p2 : p1;
				var tArray = triangle.Select(t => t.ToVec3()).ToArray();

				return entity.OnContact(point, n, tArray);
			}

			bool b1 = entity1?.OnContact(entity2, body2, p1, -n) ?? true;
			bool b2 = entity2?.OnContact(entity1, body1, p2, n) ?? true;

			// Either entity can negate the contact.
			return b1 && b2;
		}

		/*
		private void StartRound()
		{
			var spawn = course.CurrentHole.Spawn;
			var golfball = new Golfball(coordinator.LocalPlayers[0], putter);

			scene.Add(golfball);
			golfball.Respawn(spawn);
			putter.Golfball = golfball;

			var view = Camera.GetController<FollowView>();
			view.Target = golfball;
		}
		*/

		public override void Dispose()
		{
			putter.Dispose();
			snapper.Dispose();
		}

		public override void Update()
		{
			var balls = scene.GetEntities<Golfball>(EntityGroups.Golfball);
			//balls.ForEach(b => b.JellyPrecompute());
			//jellyMesh.Precompute();

			//world.Step(Game.DeltaTime, false);
			scene.Update();
			space.Update();
			//round.Update();
			Camera.Update();
			coordinator.Update();
			//jellyMesh.Update();

			//var list = Canvas.GetElement<DebugView>().GetGroup("Players");
			//list.AddRange(coordinator.Players.Select(p => $"{p.Id.Name}#{p.Id.Tag} ({p.Strokes}, {p.IsHoled})"));
			//list.Add("Connections: " + Peer.Connections);
		}

		public override void Draw(float t)
		{
			/*
			var ball = scene.GetEntities<Golfball>(EntityGroups.Golfball)[0];
			var surface = ball.Surfaces[0];
			var q = Utilities.ComputeFlatProjection(surface.Normal);
			var points = surface.Points.Select(p => p * q).ToArray();
			var bP = ball.Position * q;

			surface.Project(ball.Position, out var result);

			scene.Primitives.DrawTriangle(points, Color.White);
			scene.Primitives.DrawLine(bP, points[0], Color.Gold);
			scene.Primitives.DrawLine(bP, result, Color.Green);
			*/

			//DrawWireframe(course.Mesh);
			scene.Draw(Camera, t);
			scene.Primitives.Flush();
			//jellyMesh.Draw(scene.Primitives);
			jitterVisualizer.Draw();
			spaceVisualizer.Draw();
		}

		private void DrawWireframe(Mesh mesh)
		{
			var points = mesh.Points;
			var vertices = mesh.Vertices;
			var indexes = mesh.Indices;

			for (int i = 0; i < indexes.Length; i += 3)
			{
				var p0 = points[vertices[indexes[i]].x];
				var p1 = points[vertices[indexes[i + 1]].x];
				var p2 = points[vertices[indexes[i + 2]].x];
				var array = new []
				{
					p0, p1, p2
				};

				scene.Primitives.DrawTriangle(array, Color.Cyan);
			}
		}
	}
}
