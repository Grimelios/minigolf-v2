﻿using Engine;
using Engine.Core;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Utility;
using GlmSharp;
using static Engine.GLFW;

namespace Minigolf
{
	public class SweepTester
	{
		private vec3 target;
		private vec3[] triangle;

		private PrimitiveRenderer3D primitives;

		public SweepTester(PrimitiveRenderer3D primitives)
		{
			this.primitives = primitives;

			triangle = new []
			{
				new vec3(-1, 1, 1),
				new vec3(1, 1.5f, 2),
				new vec3(-2, 2, 3)
			};

			InputProcessor.Add(data =>
			{
				const float Speed = Game.DeltaTime * 3;

				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				bool x = keyboard.Query(GLFW_KEY_X, InputStates.Held);
				bool y = keyboard.Query(GLFW_KEY_Y, InputStates.Held);
				bool z = keyboard.Query(GLFW_KEY_Z, InputStates.Held);
				bool up = keyboard.Query(GLFW_KEY_UP, InputStates.Held);
				bool down = keyboard.Query(GLFW_KEY_DOWN, InputStates.Held);

				if (x && up ^ down)
				{
					target.x += (up ? 1 : -1) * Speed;
				}

				if (y && up ^ down)
				{
					target.y += (up ? 1 : -1) * Speed;
				}

				if (z && up ^ down)
				{
					target.z += (up ? 1 : -1) * Speed;
				}
			});
		}

		public void Draw()
		{
			const float Length = 0.2f;

			Utilities.DistanceSquaredToTriangle(target, triangle, out var result);

			primitives.DrawTriangle(triangle, Color.Purple);
			primitives.DrawLine(target + vec3.UnitX * Length, target - vec3.UnitX * Length, Color.Red);
			primitives.DrawLine(target + vec3.UnitY * Length, target - vec3.UnitY * Length, Color.Green);
			primitives.DrawLine(target + vec3.UnitZ * Length, target - vec3.UnitZ * Length, Color.Cyan);
			primitives.DrawLine(target, result, Color.White);
		}
	}
}
