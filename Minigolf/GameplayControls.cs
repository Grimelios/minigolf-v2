﻿using System.Collections.Generic;
using Engine.Input.Data;
using static Engine.GLFW;

namespace Minigolf
{
	public class GameplayControls
	{
		public GameplayControls()
		{
			Respawn = new List<InputBind>
			{
				new InputBind(InputTypes.Keyboard, GLFW_KEY_R)
			};
		}

		public List<InputBind> Respawn { get; }
	}
}
