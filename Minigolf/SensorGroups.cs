﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf
{
	public static class SensorGroups
	{
		public const uint Golfball = 1<<0;
		public const uint Hole = 1<<1;
		public const uint None = 0;
	}
}
