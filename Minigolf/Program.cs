﻿using GlmSharp;

namespace Minigolf
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			var name = args[0];
			var x = int.Parse(args[1]);// + 1920;
			var y = int.Parse(args[2]);
			var port = int.Parse(args[3]);

			var game = new MainGame(name, x, y, port);
			game.Run();
		}
	}
}
