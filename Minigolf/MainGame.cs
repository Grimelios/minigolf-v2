﻿using System.Collections.Generic;
using System.Diagnostics;
using Engine;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Graphics._3D;
using Engine.Messaging;
using Engine.UI;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Minigolf.Jelly;
using Minigolf.Loops;
using Minigolf.Networking;
using Minigolf.Settings;
using Minigolf.UI.Menus;
using static Engine.GL;
using static Engine.GLFW;

namespace Minigolf
{
	public class MainGame : Game
	{
		private Gamestates currentState;
		private Gamestates nextState;

		private string playerName;

		private GameLoop activeLoop;
		private Camera3D camera;
		private Canvas canvas;
		private SpriteBatch sb;
		private RenderTarget mainTarget;
		private MenuManager menuManager;
		private MinigolfPeer peer;
		private Sprite mainSprite;
		private MasterSettings settings;
		
		// TODO: Should pause functionality be moved to the gameplay loop?
		private bool isPaused;
		private bool isPauseToggled;
		private bool justPaused;

		// When the game is paused, the current interpolation value needs to be stored (to prevent visible jitter while
		// paused).
		private float tPaused;

		public MainGame(string playerName, int x, int y, int port) : base("Minigolf", "Minigolf", new ivec2(x, y))
		{
			this.playerName = playerName;
			
			glClearColor(0, 0, 0, 1);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glPrimitiveRestartIndex(Constants.RestartIndex);
			glfwSetInputMode(window.Address, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

			// The peer is always started immediately (such that invites and join requests can be processed right away,
			// even before the title screen is visible).
			peer = new MinigolfPeer(port);
			peer.Start();

			camera = new Camera3D();
			canvas = new Canvas(this);
			canvas.Peer = peer;
			sb = new SpriteBatch();
			settings = new MasterSettings();
			menuManager = new MenuManager(this);
			PauseElements = new List<CanvasElement>();

			mainTarget = new RenderTarget(Resolution.RenderWidth, Resolution.RenderHeight, RenderTargetFlags.Color |
				RenderTargetFlags.Depth);
			mainSprite = new Sprite(mainTarget, null, Alignments.Left | Alignments.Top);
			mainSprite.Mods = SpriteModifiers.FlipVertical;

			FocusCallback = isFocused =>
			{
				// TODO: Verify the ordering of events (ideally, the focus callback should be called prior to gamestate and other processing).
				if (!isFocused)
				{
					OnUnfocus();
				}
			};

			AdvanceGamestate(Gamestates.Gameplay);

			MessageSystem.Subscribe(this, CoreMessageTypes.ResizeWindow, data =>
			{
				mainSprite.ScaleTo(Resolution.WindowWidth, Resolution.WindowHeight, false);
			});
			
			MessageSystem.Subscribe(this, CoreMessageTypes.Gamestate, data =>
			{
				var state = (Gamestates)data;

				Debug.Assert(state != Gamestates.Splash, "Can't transition to the splash loop (it's only meant to " +
					"be visible on launch.");

				// This implementation means that if multiple gamestate messages are sent on a single frame, the last
				// state will take priority (although that should never happen in practice).
				if (state != currentState)
				{
					nextState = state;
				}
			});

			// Calling this function here is required to ensure that all classes receive initial resize messages.
			MessageSystem.ProcessChanges();
			MessageSystem.Send(CoreMessageTypes.ResizeRender, Resolution.RenderDimensions);
			MessageSystem.Send(CoreMessageTypes.ResizeWindow, Resolution.WindowDimensions);
		}

		// While the game is paused, the entire game is effectively frozen (including most UI elements), *except* for
		// elements in this list. Similarly, for rendering, interpolation is frozen (except for this list).
		public List<CanvasElement> PauseElements { get; }

		public void OnUnfocus()
		{
			// TODO: Verify it's not possible to unfocus the game (causing a pause) and unpause on the same frame.
			//if (settings.General.ShouldPauseOnUnfocus)
			{
				//isPauseToggled = true;
			}
		}

		public void TogglePause()
		{
			isPauseToggled = true;
		}

		protected override void Update()
		{
			peer.Refresh();
			menuManager.Update();

			if (isPaused)
			{
				PauseElements.ForEach(e => e.Update());
			}
			else
			{
				activeLoop.Update();
				canvas.Update();
			}

			// TODO: Verify ordering between pausing and gamestate changes.
			// Pausing incurs an intentional one-frame buffer. On the frame the game is paused, the game still updates
			// one additional time. Similarly, on the frame the game is unpaused, the game doesn't begin updating again
			// until the *next* frame.
			if (isPauseToggled)
			{
				// The toggle flag is set back to false in the Draw call (in order to properly capture tPaused).
				isPaused = !isPaused;
				isPauseToggled = false;
				justPaused = isPaused;

				// TODO: Hide the cursor until the mouse is actually used (similar to Ori, which hides the cursor most of the time while using a controller).
				// The cursor becomes visible while in a menu.
				ToggleCursor(isPaused);
			}

			// Gamestate changes don't apply until the end of the current frame.
			if (nextState != Gamestates.None && nextState != currentState)
			{
				AdvanceGamestate(nextState);
			}

			MessageSystem.ProcessChanges();
		}

		private void AdvanceGamestate(Gamestates gamestate)
		{
			menuManager.Clear();
			activeLoop = CreateLoop(gamestate);
			activeLoop.Initialize(this);
			currentState = gamestate;
			nextState = Gamestates.None;

			settings.Apply();
		}

		private GameLoop CreateLoop(Gamestates state)
		{
			GameLoop loop = null;

			switch (state)
			{
				case Gamestates.Gameplay:
					loop = new GameplayLoop(playerName);
					break;

				case Gamestates.Title:
					//loop = new TitleLoop();
					break;

				case Gamestates.Splash:
					//loop = new SplashLoop();
					break;
			}

			loop.Camera = camera;
			loop.Canvas = canvas;
			loop.Peer = peer;
			loop.MenuManager = menuManager;
			loop.Settings = settings;
			loop.SpriteBatch = sb;

			return loop;
		}

		protected override void Draw(float t)
		{
			if (justPaused)
			{
				tPaused = t;
				justPaused = false;
			}

			var tActual = t;

			if (isPaused)
			{
				t = tPaused;
			}

			// Render 3D targets.
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glDepthFunc(GL_LEQUAL);

			camera.Recompute(t);
			activeLoop.DrawTargets(t);
			mainTarget.Apply();
			activeLoop.Draw(t);

			// Render 2D targets.
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
			glDepthFunc(GL_NEVER);

			canvas.DrawTargets(sb, t);

			// Draw to the main screen.
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, (uint)Resolution.WindowWidth, (uint)Resolution.WindowHeight);

			sb.ApplyTarget(null);
			mainSprite.Draw(sb, t);
			canvas.Draw(sb, t);

			if (isPaused)
			{
				// TODO: Handle render targets for pause elements (if needed for any of them).
				// While paused, pause-related UI elements (like the pause menu) should still be interpolated
				// properly (rather than frozen like everything else).
				PauseElements.ForEach(e => e.Draw(sb, tActual));
			}

			sb.Flush();
		}
	}
}
