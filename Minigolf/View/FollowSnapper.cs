﻿using System;
using Engine.Input;
using Engine.Input.Data;

namespace Minigolf.View
{
	public class FollowSnapper : IControllable
	{
		private FollowView view;

		public FollowSnapper(FollowView view)
		{
			this.view = view;

			InputProcessor.Add(this, 40);
		}

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			view.ProcessInput(data);

			return InputFlowTypes.BlockingUnpaused;
		}

		public void Dispose()
		{
			InputProcessor.Remove(this);
		}
	}
}
