﻿using System.Diagnostics;
using Engine.Input;
using Engine.Props;
using Engine.View;
using GlmSharp;
using Minigolf.Entities;
using Minigolf.Settings;

namespace Minigolf.View
{
	public class FollowView : OrbitView
	{
		private ControlSettings settings;

		private float shift;
		private float minDistance;
		private float maxDistance;
		private float distance;

		public FollowView(ControlSettings settings)
		{
			this.settings = settings;

			settings.ApplyEvent += OnApply;
		}

		public Golfball Target { get; set; }

		private void OnApply(ControlSettings settings)
		{
			sensitivity = settings.MouseSensitivity;
			invertX = settings.InvertX;
			invertY = settings.InvertY;
		}

		public override void Dispose()
		{
			settings.ApplyEvent -= OnApply;

			base.Dispose();
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"follow.shift",
				"follow.min.distance",
				"follow.max.distance",
				"follow.max.pitch"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			var localMin = results["follow.min.distance"];
			var localMax = results["follow.max.distance"];

			if (localMin > localMax)
			{
				message = "Minimum follow distance can't be greater than the maximum distance.";

				return false;
			}

			shift = results["follow.shift"];
			minDistance = localMin;
			maxDistance = localMax;
			maxPitch = results["follow.max.pitch"];
			distance = maxDistance;
			message = null;

			return true;
		}

		public override void Update()
		{
			Debug.Assert(Target != null, "The follow view must have a non-null target.");

			// TODO: Since the camera is shifted slighty upward, experiment with subtly modifying follow distance based on pitch.
			var aim = quat.FromAxisAngle(Pitch, vec3.UnitX) * quat.FromAxisAngle(Yaw, vec3.UnitY);
			var v = new vec3(0, shift, 0);
			var p = Target.Position;
			var eye = p + new vec3(0, 0, -distance) * aim + v;

			Camera.Position.SetValue(eye, true);
			Camera.Orientation.SetValue(mat4.LookAt(eye, p + v, vec3.UnitY).ToQuaternion, true);
		}
	}
}
