﻿using System;
using System.Diagnostics;

namespace Minigolf.Players
{
	[DebuggerDisplay("{Name}#{Tag}")]
	public class PlayerId
	{
		// TODO: Use a centralized source of RNG instead.
		private static Random random = new Random();

		public static int GenerateTag()
		{
			return random.Next(9000) + 1000;
		}

		public PlayerId(string name, int tag = 0)
		{
			Debug.Assert(!string.IsNullOrEmpty(name), "Player name can't be null or empty.");
			Debug.Assert(tag == 0 || (tag >= 1000 && tag <= 9999), "Player tag must be a four-digit positive " +
				"integer (or zero to auto-generate a new tag).");

			Name = name;
			Tag = tag > 0 ? tag : GenerateTag();
		}

		public string Name { get; }
		public int Tag { get; }

		public override bool Equals(object obj)
		{
			var other = obj as PlayerId;

			return Name == other.Name && Tag == other.Tag;
		}

		public override int GetHashCode()
		{
			return new Tuple<string, int>(Name, Tag).GetHashCode();
		}
	}
}
