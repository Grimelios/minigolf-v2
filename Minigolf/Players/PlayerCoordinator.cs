﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Entities;
using Engine.Interfaces;
using Engine.Networking;
using Lidgren.Network;
using Minigolf.Data;
using Minigolf.Entities;
using Minigolf.Jelly;
using Minigolf.Networking;
using Minigolf.Networking.Packets;
using Minigolf.View;

namespace Minigolf.Players
{
	public class PlayerCoordinator : IDynamic, INetworked
	{
		private Scene scene;
		private MinigolfPeer peer;
		private Course course;
		private Round round;
		private Putter putter;
		private Dictionary<PlayerId, Player> players;

		public PlayerCoordinator(Scene scene, MinigolfPeer peer, Course course, Round round, Putter putter)
		{
			this.scene = scene;
			this.putter = putter;
			this.course = course;
			this.round = round;
			this.peer = peer;

			players = new Dictionary<PlayerId, Player>();

			peer.Subscribe((uint)PacketTypes.Ball, this);
			peer.Subscribe((uint)PacketTypes.Player, this);
			peer.Coordinator = this;

			round.RoundStartEvent += OnRoundStart;
			round.HoleAdvanceEvent += OnHoleAdvance;
		}

		public event Action<Player> PlayerAddEvent;
		public event Action<Player> PlayerRemoveEvent;

		public List<NetworkHandle> NetworkHandles { get; set; }

		// An array is used in order to accommodate local (i.e. splitscreen) multiplayer in the future.
		public Player[] LocalPlayers => players.Values.Where(p => p.IsLocal).ToArray();
		public Player[] Players => players.Values.ToArray();

		// The "leader" similar to a host, but not an actual network host (since this game uses peer-to-peer). The
		// leader still has authority to coordinate certain actions, though.
		public bool IsLeader { get; set; }

		public Packet OutgoingHail { get; private set; }

		public Player AddPlayer(string name, int tag = 0, NetConnection connection = null)
		{
			// Tags are internal and used to differentiate players with the same name. As such, if any players with the
			// same name already exist, a new, distinct tag must be generated.
			var keys = players.Keys.Where(k => k.Name == name).ToList();

			if (keys.Count > 0)
			{
				// When a player launches their game, a random player tag is generated (and forwarded to remote
				// machines when a lobby is joined). In most cases, this tag can be used directly (since it's
				// extremely likely to already be unique, even if there are other players with the same name). This
				// loop still protects against the rare case where it's not, though.
				while (tag == 0 || keys.Any(k => k.Tag == tag))
				{
					tag = PlayerId.GenerateTag();
				}
			}
			else if (tag == 0)
			{
				tag = PlayerId.GenerateTag();
			}

			var player = new Player(name, tag, connection, this);
			players.Add(player.Id, player);

			if (round.HasStarted)
			{
				SpawnBall(player);
			}

			return player;
		}

		public void Process(Packet packet)
		{
			switch ((PacketTypes)packet.PacketType)
			{
				case PacketTypes.Ball: Process((BallPacket)packet);
					break;

				case PacketTypes.Player: Process((PlayerPacket)packet);
					break;
			}
		}

		private void Process(PlayerPacket packet)
		{
			var id = packet.Id;
			var flags = packet.Flags;
			var connection = packet.Connection;

			// This means that a new player is joining (i.e. they connected to this machine directly).
			if ((flags & PlayerPacketFlags.Join) > 0)
			{
				var player = AddPlayer(id.Name, id.Tag, connection);

				// TODO: Pass connection data as well.
				// TODO: If the round has started, additional information needs to be sent back (like scores).
				// When a new player joins, the existing list of players is sent back.
				packet.Flags = PlayerPacketFlags.Forward;
				packet.Ids = players.Where(p => p.Value != player).Select(p => p.Value.Id).ToArray();

				// This scenario is extremely rare. It can only occur if 1) the newly-joined player has the same name
				// as at least one existing player in the lobby, and 2) the randomly-generated tag is also the same. If
				// both are true, the new tag needs to be sent back to the joining player.
				if (player.Id.Tag != id.Tag)
				{
					// When returning the new tag, the ID array (used above to forward players) is reused.
					packet.Id = player.Id;
					packet.Flags |= PlayerPacketFlags.Id;
				}
				else
				{
					packet.Id = null;
				}

				OutgoingHail = packet;

				// The new player also needs to be forwarded.
				var newPacket = new PlayerPacket();
				newPacket.Ids = new [] { player.Id };
				newPacket.Flags = PlayerPacketFlags.Forward;

				peer.Send(newPacket, NetDeliveryMethod.ReliableUnordered, connection);

				return;
			}

			// This means that a remote player is leaving.
			if ((flags & PlayerPacketFlags.Leave) > 0)
			{
				return;
			}

			// This means that an invite was received from a remote player.
			if ((flags & PlayerPacketFlags.Invite) > 0)
			{
				return;
			}

			// This means that a forwarded list of players was received.
			if ((flags & PlayerPacketFlags.Forward) > 0)
			{
				// This means that the rare scenario described above occurred (the local player's ID matched an
				// existing player, so an updated tag was sent back).
				if ((flags & PlayerPacketFlags.Id) > 0)
				{
					players[id].Id = id;
				}

				foreach (var ID in packet.Ids)
				{
					// TODO: Pull connection as well.
					AddPlayer(ID.Name, ID.Tag);
				}

				return;
			}

			if ((flags & PlayerPacketFlags.Stroke) > 0)
			{
				// TODO: Update the UI.
				players[id].Strokes = packet.Strokes;
			}

			if ((flags & PlayerPacketFlags.Hole) > 0)
			{
				players[id].IsHoled = true;
			} 
		}
		
		private void Process(BallPacket packet)
		{
			var ball = players[packet.PlayerId].Golfball;
			ball.SetPosition(packet.Position, false);
			ball.SetOrientation(packet.Orientation, false);
			ball.LinearVelocity = packet.LinearVelocity;
			ball.AngularVelocity = packet.AngularVelocity;
		}

		private void SpawnBall(Player player)
		{
			var golfball = new Golfball(player, putter, course.Mesh);
			scene.Add(golfball);

			if (player.IsLocal)
			{
				var view = scene.Camera.GetController<FollowView>();
				view.Target = golfball;
				putter.Golfball = golfball;
			}
		}

		private void OnRoundStart()
		{
			foreach (var p in players.Values)
			{
				SpawnBall(p);
			}
		}

		// This is called whenever a player finishes a hole. When all players have finished, the round is progressed to
		// the next hole (or ended after the last hole).
		public void OnHoleFinished(Player player)
		{
			// TODO: Should this be sent if all players have finished? Might be redundant.
			if (player.IsLocal)
			{
				var packet = player.Packet;
				packet.Hole = round.CurrentHole.Index;
				packet.Strokes = putter.Strokes;
				packet.Flags |= PlayerPacketFlags.Hole;
			}

			if (IsLeader && players.Values.All(p => p.IsHoled))
			{
				round.OnHoleFinished();
			}
		}

		private void OnHoleAdvance(Hole hole)
		{
			var spawn = hole.Spawn;

			foreach (var player in players.Values)
			{
				player.IsHoled = false;
				player.Strokes = 0;
				putter.Strokes = 0;

				// TODO: Could precompute the ground to avoid multiple (duplicative) downward raycasts.
				player.Golfball.Respawn(spawn);
			}
		}

		public void Dispose()
		{
			peer.Unsubscribe(this);
		}

		public void Update()
		{
			foreach (var player in players.Values)
			{
				var packet = player.Packet;

				if (packet.Flags != PlayerPacketFlags.None)
				{
					peer.Send(packet, NetDeliveryMethod.ReliableSequenced);
					packet.Flags = PlayerPacketFlags.None;
				}
			}
		}
	}
}
