﻿using System.Diagnostics;
using Lidgren.Network;
using Minigolf.Entities;
using Minigolf.Networking.Packets;

namespace Minigolf.Players
{
	[DebuggerDisplay("{Id.Name}#{Id.Tag}")]
	public class Player
	{
		private PlayerPacket packet;
		private PlayerCoordinator coordinator;

		public Player(string name, int tag, NetConnection connection, PlayerCoordinator coordinator)
		{
			this.coordinator = coordinator;

			Id = new PlayerId(name, tag);
			packet = new PlayerPacket(Id);
			Connection = connection;
		}

		public PlayerId Id { get; set; }
		public PlayerPacket Packet => packet;
		public Golfball Golfball { get; set; }
		public NetConnection Connection { get; set; }

		public int Strokes { get; set; }

		public bool IsLocal => Connection == null;
		public bool IsHoled
		{
			get => Golfball.IsHoled;
			set
			{
				Golfball.IsHoled = value;

				if (value)
				{
					coordinator.OnHoleFinished(this);
				}
			}
		}
	}
}
