﻿using System;

namespace Minigolf.Settings
{
	public class ControlSettings
	{
		public ControlSettings()
		{
			// TODO: Load settings from a file.
			MouseSensitivity = 50;
		}

		public event Action<ControlSettings> ApplyEvent;

		public float MouseSensitivity { get; set; }

		public bool InvertX { get; set; }
		public bool InvertY { get; set; }

		public void Apply()
		{
			ApplyEvent?.Invoke(this);
		}
	}
}
