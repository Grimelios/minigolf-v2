﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Settings
{
	public class MasterSettings
	{
		public MasterSettings()
		{
			Control = new ControlSettings();
		}

		public ControlSettings Control { get; }

		public void Apply()
		{
			// TODO: Only apply settings that have changed.
			Control.Apply();
		}
	}
}
