﻿using System.Diagnostics;
using Engine.Input;
using Engine.Input.Data;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Lidgren.Network;
using Minigolf.Data;
using Minigolf.Entities;
using Minigolf.Networking;
using Minigolf.Networking.Packets;
using Minigolf.Players;
using Minigolf.UI.Hud;
using Minigolf.View;
using static Engine.GLFW;

namespace Minigolf
{
	public class Putter : IControllable
	{
		public delegate void StrokeHandler(int strokes, int limit);

		private MinigolfPeer peer;
		private Camera3D camera;
		private Round round;
		private Golfball golfball;
		private FollowView view;
		private StrokeDisplay strokeDisplay;
		private GameplayControls controls;

		private int strokes;

		public Putter(MinigolfPeer peer, Camera3D camera, Round round, StrokeDisplay strokeDisplay)
		{
			Debug.Assert(round != null, "Round must not be null (when passed to the putter).");

			this.peer = peer;
			this.camera = camera;
			this.round = round;
			this.strokeDisplay = strokeDisplay;

			view = camera.GetController<FollowView>();
			controls = new GameplayControls();

			InputProcessor.Add(this, 30);
		}

		public event StrokeHandler StrokeEvent;

		public Golfball Golfball
		{
			set => golfball = value;
		}

		public int Strokes
		{
			get => strokes;
			set
			{
				Debug.Assert(value >= 0, "Strokes can't be negative.");

				strokes = value;
				strokeDisplay.Strokes = value;
			}
		}

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			if (golfball == null || golfball.IsHoled || strokes == round.StrokeLimit)
			{
				return InputFlowTypes.Passthrough;
			}

			// TODO: If the ball is in the hole, transfer control to the snapper instead.
			view.ProcessInput(data);

			if (golfball.IsRespawnAvailable && data.Query(controls.Respawn, InputStates.PressedThisFrame))
			{
				golfball.ResetToCheckpoint();
			}
			else if (golfball.IsStopped && data.Query(InputTypes.Keyboard, GLFW_KEY_I, InputStates.PressedThisFrame))
			{
				/*
				var v = -vec3.UnitZ * camera.Orientation.ResultValue;
				v = Utilities.Normalize(new vec3(v.x, 0, v.z)) * 8;

				var player = golfball.Player;

				golfball.Hit(v);
				Strokes++;
				player.Strokes = strokes;
				StrokeEvent?.Invoke(strokes, round.StrokeLimit);

				var packet = player.Packet;
				packet.Strokes = strokes;
				packet.Flags |= PlayerPacketFlags.Stroke;
				*/
			}

			return InputFlowTypes.BlockingUnpaused;
		}

		public void Dispose()
		{
			InputProcessor.Remove(this);
		}
	}
}
