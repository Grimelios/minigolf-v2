﻿using Engine.Interfaces;
using Engine.Timing;
using GlmSharp;

namespace Minigolf.Cinematics
{
	public class PanEvent : IDynamic
	{
		private SingleTimer timer;

		public PanEvent(vec3 positionStart, vec3 positionEnd, quat orientationStart, quat orientationEnd,
			float duration)
		{
			timer = new SingleTimer(t =>
			{
				Position = positionEnd;
				Orientation = orientationEnd;
			}, duration, TimerFlags.None);

			timer.Tick = t =>
			{
				Position = vec3.Lerp(positionStart, positionEnd, t);
				Orientation = quat.SLerp(orientationStart, orientationEnd, t);
			};
		}

		public vec3 Position { get; private set; }
		public quat Orientation { get; private set; }

		public void Update()
		{
			timer.Update();
		}
	}
}
