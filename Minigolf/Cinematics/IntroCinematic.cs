﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Interfaces;
using Engine.Timing;
using Engine.View;

namespace Minigolf.Cinematics
{
	public class IntroCinematic : IDynamic
	{
		private Camera3D camera;
		private List<PanEvent> events;
		private PanEvent currentEvent;
		private RepeatingTimer timer;
		private int[] indexes;
		private int currentIndex;

		public IntroCinematic(Camera3D camera, List<PanEvent> events)
		{
			const int ClipCount = 3;
			const int ClipDuration = 3;

			this.camera = camera;
			this.events = events;

			// TODO: Synchronize random clips among players (i.e. all players should see the same set of clips in the same order).
			var random = new Random();

			indexes = new int[ClipCount];

			for (int i = 0; i < indexes.Length; i++)
			{
				int index;

				do
				{
					index = random.Next(events.Count);
				}
				while (indexes.Contains(index));

				indexes[i] = i;
			}

			timer = new RepeatingTimer(t =>
			{
				currentIndex++;

				if (currentIndex < indexes.Length)
				{
					currentEvent = events[currentIndex];
				}

				return currentIndex == indexes.Length;
			}, ClipDuration);

			currentEvent = events[0];
		}

		public void Update()
		{
			timer.Update();
			currentEvent.Update();

			camera.Position.SetValue(currentEvent.Position, true);
			camera.Orientation.SetValue(currentEvent.Orientation, true);
		}
	}
}
