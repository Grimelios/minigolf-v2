﻿using Minigolf.Players;

namespace Minigolf.Cinematics
{
	public class Highlight
	{
		private PlayerId featuring;

		public Highlight(PlayerId featuring)
		{
			this.featuring = featuring;
		}
	}
}
