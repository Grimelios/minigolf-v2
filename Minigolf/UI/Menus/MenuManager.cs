﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Engine;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;

namespace Minigolf.UI.Menus
{
	public class MenuManager : IControllable, IDynamic
	{
		private MainGame game;
		private List<Menu> menus;
		private Menu activeMenu;
		private MenuControls controls;

		// The pause menu is tracked explicitly in order to track when pause is toggled.
		private PauseMenu pauseMenu;

		public MenuManager(MainGame game)
		{
			this.game = game;

			controls = new MenuControls();
			menus = new List<Menu>();
		
			InputProcessor.Add(this, 20);
		}

		public void Add(Menu menu)
		{
			Debug.Assert(menu != null, "Can't add a null menu.");
			Debug.Assert(!menus.Contains(menu), "Duplicate menu added.");

			menus.Add(menu);
			pauseMenu = menu as PauseMenu;
		}

		public void Clear()
		{
			menus.Clear();
		}
		
		public InputFlowTypes ProcessInput(FullInputData data)
		{
			// This ensures that a menu doesn't process input on the frame it becomes visible.
			var oldMenu = activeMenu;

			// This means that the gameplay loop must be active (since that's the only loop where pausing is possible).
			if (pauseMenu != null)
			{
				if (data.Query(controls.Pause, InputStates.PressedThisFrame))
				{
					activeMenu = activeMenu != null ? pauseMenu : null;
					pauseMenu.IsVisible = !pauseMenu.IsVisible;
					game.TogglePause();

					// Other input isn't processed on the frame the pause menu becomes visible.
					return InputFlowTypes.BlockingPaused;
				}
			}

			// TODO: Verify logic when multiple menus are involved (especially in the gameplay loop, where special pause handling is run).
			Debug.Assert(menus.Count(m => m.IsVisible) <= 1, "Multiple menus shouldn't be visible at once.");

			activeMenu = menus.FirstOrDefault(m => m.IsVisible);

			if (activeMenu != null && oldMenu == activeMenu)
			{
				activeMenu.ProcessInput(data, controls);

				return InputFlowTypes.BlockingPaused;
			}

			return InputFlowTypes.Passthrough;
		}

		public void Dispose()
		{
			InputProcessor.Remove(this);
		}

		public void Update()
		{
			activeMenu?.Update();
		}
	}
}
