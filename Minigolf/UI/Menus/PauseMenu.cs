﻿using Engine.Messaging;

namespace Minigolf.UI.Menus
{
	// TODO: If the player clicks on Unpause (to unpause the game), that shouldn't *also* trigger an attack.
	public class PauseMenu : Menu
	{
		private MainGame game;

		public PauseMenu(MainGame game)
		{
			this.game = game;

			Initialize(new []
			{
				"Unpause",
				"Editor",
				"Settings",
				"Exit"
			});
			
			IsVisible = false;
		}

		public override void Submit(int index)
		{
			switch (index)
			{
				// Unpause
				case 0:
					IsVisible = false;
					game.TogglePause();

					break;

				// Editor
				case 1:
					break;

				// Settings
				case 2:
					break;

				// Exit
				case 3:
					MessageSystem.Send(CoreMessageTypes.Exit);

					break;
			}
		}
	}
}
