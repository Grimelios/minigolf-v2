﻿using Engine.Core;
using Engine.Core._2D;
using Engine.UI;

namespace Minigolf.UI.Hud
{
	public class HoleResultDisplay : CanvasElement
	{
		private SpriteText text;
		private FadeComponent fadeComponent;

		private string[] strings;

		public override void Initialize()
		{
			text = Attach(new SpriteText("Debug", null, Alignments.Center));

			strings = new []
			{
				"Ostrich",
				"Condor",
				"Albatross",
				"Eagle",
				"Birdie",
				"Par",
				"Bogey",
				"Double Bogey",
				"Triple Bogey"
			};

			fadeComponent = Components.Add(new FadeComponent(0.2f, 2.5f));

			base.Initialize();
		}

		public void Refresh(int strokes, int par)
		{
			string value;

			switch (strokes)
			{
				// This means that the player sunk the wrong hole.
				case -1:
					value = "Wrong hole!";

					break;

				case 1:
					value = "Hole In One!";

					break;

				default:
				{
					var delta = strokes - par;

					value = delta <= 5 ? strings[delta + 5] : "Bogey +" + delta;

					break;
				}
			}

			text.Value = value;
			fadeComponent.Start();
		}

		public override void Update()
		{
			base.Update();

			text.Color.SetValue(fadeComponent.Color, true);
		}
	}
}
