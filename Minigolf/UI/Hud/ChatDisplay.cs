﻿using System.Collections.Generic;
using Engine;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Networking;
using Engine.UI;
using Minigolf.Networking.Packets;

namespace Minigolf.UI.Hud
{
	public class ChatDisplay : CanvasElement, INetworked
	{
		private List<SpriteText> textList;
		private SpriteFont font;

		public ChatDisplay()
		{
			font = ContentCache.GetFont("Debug");
			textList = new List<SpriteText>();
		}

		public List<NetworkHandle> NetworkHandles { get; set; }

		public override void Initialize()
		{
			var peer = Canvas.Peer;
			peer.Subscribe((uint)PacketTypes.Chat, this);
			peer.Subscribe((uint)PacketTypes.Player, this);

			base.Initialize();
		}

		public void Process(Packet packet)
		{
			switch ((PacketTypes)packet.PacketType)
			{
				case PacketTypes.Chat:
					Process((ChatPacket)packet);
					break;

				case PacketTypes.Player:
					Process((PlayerPacket)packet);
					break;
			}
		}

		private void Process(ChatPacket packet)
		{
		}

		private void Process(PlayerPacket packet)
		{
			var flags = packet.Flags;
			var id = packet.Id;

			if ((flags & PlayerPacketFlags.Join) > 0)
			{
				Add(id.Name + " joined.");
			}
			else if ((flags & PlayerPacketFlags.Leave) > 0)
			{
				Add(id.Name + " left.");
			}
			else if ((flags & PlayerPacketFlags.Id) > 0)
			{
				// TODO: Consider adding pronouns (and update this message accordingly).
				Add($"{id.Name} changed their name to {packet.Ids[0].Name}.");
			}
		}

		private void Add(string value)
		{
			var entry = new SpriteText(font, value, Alignments.Left | Alignments.Bottom);
			entry.Position.SetValue(Position, false);

			// TODO: Interpolate text up quickly when new messages arrive.
			foreach (var t in textList)
			{
				t.Position.SetY(t.Position.Value.y - font.Size, false);
			}

			textList.Add(entry);
		}

		public override void Draw(SpriteBatch sb, float t)
		{
			textList.ForEach(entry => entry.Draw(sb, t));

			base.Draw(sb, t);
		}
	}
}
