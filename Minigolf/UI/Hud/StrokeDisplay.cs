﻿using System.Diagnostics;
using Engine.Core._2D;
using Engine.UI;

namespace Minigolf.UI.Hud
{
	public class StrokeDisplay : CanvasElement
	{
		private SpriteText text;

		public StrokeDisplay()
		{
			text = Attach(new SpriteText("Debug", "0", Alignments.Center));
		}

		public int Strokes
		{
			set => text.Value = value.ToString();
		}
	}
}
