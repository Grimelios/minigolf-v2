﻿using Engine;
using Engine.Core._2D;
using Engine.UI;
using GlmSharp;
using Minigolf.Data;

namespace Minigolf.UI.Hud
{
	public class HoleNameDisplay : CanvasElement
	{
		public HoleNameDisplay(Round round)
		{
			var font = ContentCache.GetFont("Debug");

			var index = Attach(new SpriteText(font, null, Alignments.Center), new ivec2(0, -30));
			var name = Attach(new SpriteText(font, null, Alignments.Center));
			var par = Attach(new SpriteText(font, null, Alignments.Center), new ivec2(0, 40));

			round.HoleAdvanceEvent += hole =>
			{
				index.Value = "Hole " + (hole.Index + 1);
				name.Value = hole.Name;
				par.Value = "Par " + par;
			};
		}
	}
}
