﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine;
using Engine.Core;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;
using Engine.Physics;
using Engine.UI;
using Engine.Utility;
using GlmSharp;
using Minigolf.Entities;
using Minigolf.Physics;

namespace Minigolf.Jelly
{
	public class JellyMesh : IDynamic
	{
		// TODO: Find a better way to grab a mesh reference.
		public static JellyMesh Mesh { get; private set; }

		private Mesh mesh;
		private vec3[] vertexNormals;
		private JellySpring[] springs;
		private JellyEdge[] edges;
		private Scene scene;

		private bool shouldList;
		private bool useMassResolution;

		private vec3 impactP;
		private vec3 impactN;
		private vec3 impactB;
		private vec3 baseP;
		private vec3 baseN;
		private vec3 baseV;
		private vec3[] baseTri;

		private float impactM;

		public JellyMesh(string mesh, Scene scene) : this(ContentCache.GetMesh(mesh))
		{
			this.scene = scene;

			useMassResolution = true;

			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW.GLFW_KEY_M, InputStates.PressedThisFrame))
				{
					useMassResolution = !useMassResolution;
				}

				var camera = scene.Camera;
				var start = camera.Position.ResultValue;
				var direction = -vec3.UnitZ * camera.Orientation.ResultValue;
				var m = ContentCache.GetMesh(mesh);
				var vertices = m.Vertices;
				var indexes = m.Indices;
				var tMin = float.MaxValue;
				var tri = ivec3.Zero;

				shouldList = false;

				for (int i = 0; i < indexes.Length; i += 3)
				{
					const float Range = 1000;

					var i0 = vertices[indexes[i]].x;
					var i1 = vertices[indexes[i + 1]].x;
					var i2 = vertices[indexes[i + 2]].x;
					var p0 = springs[i0].Position;
					var p1 = springs[i1].Position;
					var p2 = springs[i2].Position;
					
					if (Utilities.Intersects(start, start + direction * Range, p0, p1, p2, out var t) && t < tMin)
					{
						tri = new ivec3(i0, i1, i2);

						impactP = start + direction * Range * t;
						impactN = Utilities.ComputeNormal(p0, p1, p2, WindingTypes.Clockwise);
						impactB = Utilities.ComputeBarycentric(impactP, p0, p1, p2);
						impactM = CastMass(impactP, impactN, direction, impactB, tri);

						tMin = t;
						shouldList = true;
					}
				}

				if (shouldList && keyboard.Query(GLFW.GLFW_KEY_L, InputStates.PressedThisFrame))
				{
					const float Mass = 5;

					var force = (direction * Mass) / impactM;
					var f0 = force * impactB.x;
					var f1 = force * impactB.y;
					var f2 = force * impactB.z;

					springs[tri.x].Position += f0;
					springs[tri.y].Position += f1;
					springs[tri.z].Position += f2;
				}
			});
		}

		public JellyMesh(Mesh mesh)
		{
			this.mesh = mesh;

			Mesh = this;

			var points = mesh.Points;
			springs = new JellySpring[points.Length];
			vertexNormals = new vec3[points.Length];

			var spread = MassPrecompute(mesh);
			var masses = ComputeMasses(vertexNormals, spread);

			for (int i = 0; i < springs.Length; i++)
			{
				var p = points[i];

				springs[i] = new JellySpring(p, masses[i], i);
			}

			var indexes = mesh.Indices;
			var vertices = mesh.Vertices;
			var eList = new List<JellyEdge>();

			for (int i = 0; i < indexes.Length; i += 3)
			{
				var s0 = springs[vertices[indexes[i]].x];
				var s1 = springs[vertices[indexes[i + 1]].x];
				var s2 = springs[vertices[indexes[i + 2]].x];

				if (!(s0.IsFixed && s1.IsFixed))
				{
					eList.Add(new JellyEdge(s0, s1));
				}

				if (!(s1.IsFixed && s2.IsFixed))
				{
					eList.Add(new JellyEdge(s1, s2));
				}

				if (!(s2.IsFixed && s0.IsFixed))
				{
					eList.Add(new JellyEdge(s2, s0));
				}
			}

			edges = eList.ToArray();
		}

		// Estimated vertex mass is based on two values: 1) raycast depth into the mesh (based on vertex normal) and 2)
		// the "spread" of normals (relative to vertex normal). That spread is represented as the average dot product
		// of triangle normals. Iimagine a vertex at the top of a tall, thin spire compared to a vertex on the surface
		// of a huge sphere. Given similar raycast distances, the spire vertex should have lower mass because adjacent
		// normals are steep enough that they enclose a smaller volume.
		private float[] MassPrecompute(Mesh mesh)
		{
			// The normal of each vertex is averaged from adjacent triangles. To ensure that the same normal isn't
			// counted multiple times, the *index* of each normal is stored instead (before averaging the actual
			// vectors).
			var sets = new HashSet<int>[mesh.Points.Length];

			for (int i = 0; i < sets.Length; i++)
			{
				sets[i] = new HashSet<int>();
			}

			var vertices = mesh.Vertices;
			var indexes = mesh.Indices;

			for (int i = 0; i < indexes.Length; i += 3)
			{
				var v0 = vertices[indexes[i]];
				var v1 = vertices[indexes[i + 1]];
				var v2 = vertices[indexes[i + 2]];

				sets[v0.x].Add(v0.z);
				sets[v1.x].Add(v1.z);
				sets[v2.x].Add(v2.z);
			}

			var normals = mesh.Normals;

			// Compute vertex normal.
			for (int i = 0; i < sets.Length; i++)
			{
				var set = sets[i];
				var n = set.Aggregate(vec3.Zero, (current, index) => current + normals[index]) / set.Count;
				
				vertexNormals[i] = Utilities.Normalize(n);
			}

			var spread = new float[sets.Length];

			// Compute spread (i.e. average dot product).
			for (int i = 0; i < spread.Length; i++)
			{
				var set = sets[i];
				var n = vertexNormals[i];
				var d = set.Sum(index => Utilities.Dot(normals[index], n)) / set.Count;

				spread[i] = d;
			}

			return spread;
		}

		private float[] ComputeMasses(vec3[] normals, float[] spread)
		{
			var points = mesh.Points;

			// It's possible for the raycasts from some vertices to never exit the other side of the mesh (for example,
			// an upward slope on the edge of a map with no backing). Those points are given a mass averaged from all
			// the rest.
			var booleans = new bool[points.Length];
			var masses = new float[points.Length];

			for (int i = 0; i < points.Length; i++)
			{
				var start = points[i];
				var direction = -normals[i];
				
				if (CastMass(start, direction, out var mass))
				{
					booleans[i] = true;

					// The mass is squared in order to increase the ratio between heavy and light vertices (through
					// testing, this generally results in more realistic mesh movement, where hitting a small pillar
					// doesn't send ripples through the entire map).
					var m = mass * spread[i];
					m *= m;
					masses[i] = m;
				}
			}

			if (booleans.Any(b => !b))
			{
				var average = masses.Where(m => m > 0).Average();

				for (int i = 0; i < booleans.Length; i++)
				{
					if (!booleans[i])
					{
						masses[i] = average;
					}
				}
			}

			return masses;
		}

		private bool CastMass(vec3 p, vec3 direction, out float mass)
		{
			const float Range = 10000;
			const float Expansion = 0.001f;

			// The t>0 check is required to account for situations where you're looking directly at a back-facing
			// triangle. This should ideally never happen in the final game, but might be possible if a portion of the
			// mesh becomes particularly deformed (or if the camera clips inside the mesh).
			if (!mesh.Raycast(p, direction, Range, out var results, (tri, point, n, t) =>
				t > 0 && Utilities.Dot(n, direction) > 0, false, Expansion))
			{
				mass = 0;

				return false;
			}

			// Connected vertices are resolved based on mass ratio (i.e. light vertices won't affect heavy ones very
			// strongly). To increase those ratios, returned mass is squared (outside this function). However, that
			// approach initially backfired since most vertex masses were <1 (which resulted in the ratios being
			// *smaller*, not bigger). To fix that, mass here is artificially increased (such that all masses are
			// guaranteed >= 1).
			mass = Utilities.Distance(p, results.Position) + 1;
			
			return true;
		}

		public float CastMass(vec3 p, vec3 n, vec3 direction, vec3 barycentric, ivec3 indexes)
		{
			// Any raycast against a deformed jelly triangle is actually transformed backwards to raycast against the
			// original mesh. The rationale is that a deformed triangle will also compress the jelly beneath it.
			var points = mesh.Points;
			var p0 = points[indexes.x];
			var p1 = points[indexes.y];
			var p2 = points[indexes.z];
			var pPrime = p0 * barycentric.x + p1 * barycentric.y + p2 * barycentric.z;
			var nPrime = Utilities.ComputeNormal(p0, p1, p2, WindingTypes.Clockwise);
			var vPrime = Utilities.Orientation(n, direction) * nPrime;

			baseP = pPrime;
			baseN = nPrime;
			baseV = vPrime;
			baseTri = new [] { p0, p1, p2 };

			if (CastMass(pPrime, vPrime, out var mass))
			{
				// For consistency, mass is squared here as well.
				return mass * mass;
			}

			// As a fallback, the mass is averaged from vertex masses. This situation should rarely (if ever) occur in
			// practice, but might be possible if a portion of the mesh becomes so deformed that, for a particualr
			// raycast, there's no exit triangle.
			var m0 = springs[indexes.x].Mass;
			var m1 = springs[indexes.y].Mass;
			var m2 = springs[indexes.z].Mass;

			return m0 * barycentric.x + m1 * barycentric.y + m2 * barycentric.z;
		}

		// TODO: Update the sweep to return barycentric coordinates (or just x value for edge T) and normal.
		public bool Sweep(Golfball ball, out float tResult)
		{
			const float Epsilon = 0.001f;

			var p = ball.Position;
			var vertices = mesh.Vertices;
			var indexes = mesh.Indices;
			var tMin = -1f;

			// It's possible (although somewhat rare) for the ball to hit multiple contacts at exactly the same time
			// (or close enough to exact using the epsilon value above).
			var hits = new List<(JellySpring[] Springs, SweptCollisionTypes Type)>();

			for (int i = 0; i < indexes.Length; i += 3)
			{
				var s0 = springs[vertices[indexes[i]].x];
				var s1 = springs[vertices[indexes[i + 1]].x];
				var s2 = springs[vertices[indexes[i + 2]].x];
				var tri = new [] { s0.Position, s1.Position, s2.Position };

				// TODO: To optimize, should precompute these triangles once per frame (before any balls begin sweeping).
				var precomputed = new PrecomputedTriangle(tri, WindingTypes.Clockwise);

				if (PhysicsHelper.SweepSphere(p, ball.LinearVelocity, ball.Radius, precomputed, vec3.Zero, out var t,
					out _, out var type))
				{
					// Very close hits (in terms of t) are counted as hitting at the same time.
					var shouldRecord = tMin < 0 || t < tMin || Math.Abs(t - tMin) <= Epsilon;

					if (!shouldRecord)
					{
						continue;
					}

					tMin = t;

					if (t < tMin - Epsilon)
					{
						hits.Clear();
					}

					JellySpring[] hitSprings = null;

					if (type == SweptCollisionTypes.Surface)
					{
						hitSprings = new[] { s0, s1, s2 };
					}
					else
					{
						JellySpring[] s = { s0, s1, s2 };

						switch (type)
						{
							case SweptCollisionTypes.Edge:
								var e = Utilities.ClosestEdgeIndexes(tri, p);
								hitSprings = new[] { s[e.x], s[e.y] };

								break;

							case SweptCollisionTypes.Point:
								var index = Utilities.ClosestPoint(tri, p);
								hitSprings = new[] { s[index] };

								break;
						}
					}
					
					hits.Add((hitSprings, type));
				}
			}

			if (hits.Count == 0)
			{
				tResult = 0;

				return false;
			}

			tResult = tMin;
			hits.ForEach(h => ball.OnJellyCollision(h.Springs, h.Type));

			return true;
		}

		public void Precompute()
		{
			const int Iterations = 8;

			var deltas = new RunningAverage[springs.Length];
			var positions = springs.Select(p => p.Position).ToArray();

			for (int i = 0; i < deltas.Length; i++)
			{
				deltas[i] = new RunningAverage();
			}

			for (int i = 0; i < Iterations; i++)
			{
				foreach (var edge in edges)
				{
					var s1 = edge.Spring1;
					var s2 = edge.Spring2;
					var p1 = positions[s1.Index];
					var p2 = positions[s2.Index];
					var d = Utilities.Distance(p1, p2);
					var v = (p2 - p1) / d * (d - edge.Target) * JellyConstants.KNeighbor;
					var delta1 = deltas[s1.Index];
					var delta2 = deltas[s2.Index];

					// Spring forces are resolved using mass (such that a ligher vertex doesn't unrealistically drag
					// heavier vertices with it).
					var ratio = s1.Mass / (s1.Mass + s2.Mass);

					if (s1.IsFixed)
					{
						delta2.Average(-v);
					}
					else if (s2.IsFixed)
					{
						delta1.Average(v);
					}
					else
					{
						if (useMassResolution)
						{
							delta1.Average(v * (1 - ratio));
							delta2.Average(-v * ratio);
						}
						else
						{
							delta1.Average(v / 2);
							delta2.Average(-v / 2);
						}
					}
				}

				for (int j = 0; j < deltas.Length; j++)
				{
					var delta = deltas[j];
					positions[j] += delta.Value;
					delta.Reset();
				}
			}

			for (int i = 0; i < springs.Length; i++)
			{
				var s = springs[i];
				s.Position = positions[i];
				s.Pushback = deltas[i].Value + s.Precompute(positions[i]);
			}
		}

		public void Update()
		{
			foreach (var s in springs)
			{
				if (!s.IsFixed)
				{
					s.OldPosition = s.Position;
					s.Position += s.Pushback;
				}
			}

			var list = scene.Canvas.GetElement<DebugView>().GetGroup("Jelly");

			if (shouldList)
			{
				list.Add($"Impact: {impactP.x:F3}, {impactP.y:F3}, {impactP.z:F3}");
				list.Add($"Normal: {impactN.x:F3}, {impactN.y:F3}, {impactN.z:F3}");
				list.Add($"Coords: {impactB.x:F3}, {impactB.y:F3}, {impactB.z:F3}");
				list.Add($"Mass: {impactM}");
				list.Add($"Use mass resolution: {useMassResolution}");

				var primitives = scene.Primitives;
				primitives.DrawLine(impactP, impactP + impactN * 0.5f, Color.Green);

				return;

				primitives.DrawLine(baseP, baseP + baseN * 0.5f, Color.Gold);
				primitives.DrawLine(baseP, baseP + baseV * 0.5f, Color.Purple);

				for (int i = 0; i < 3; i++)
				{
					var p0 = baseTri[i];
					var p1 = baseTri[(i + 1) % 3];

					primitives.DrawLine(p0, p1, new Color(50));
				}
			}
			else
			{
				list.Clear();
			}
		}

		public void Draw(PrimitiveRenderer3D primitives)
		{
			var indexes = mesh.Indices;
			var vertices = mesh.Vertices;

			/*
			for (int i = 0; i < indexes.Length; i += 3)
			{
				var s0 = springs[vertices[indexes[i]].x];
				var s1 = springs[vertices[indexes[i + 1]].x];
				var s2 = springs[vertices[indexes[i + 2]].x];

				DrawLine(s0, s1);
				DrawLine(s1, s2);
				DrawLine(s2, s0);
			}
			*/

			//primitives.DrawLine(rayEnd, rayEnd + rayN * 0.5f, Color.Green);

			var max = springs.Max(s => s.Mass);

			foreach (var edge in edges)
			{
				var s1 = edge.Spring1;
				var s2 = edge.Spring2;
				var t = (s1.Mass + s2.Mass) / 2 / max;
				var f = 255 - Utilities.Lerp(0f, 255f, t);
				f *= f;

				var c = new Color((byte)f);

				primitives.DrawLine(s1.Position, s2.Position, c);
			}
		}

		private class RunningAverage
		{
			private int count;

			public vec3 Value { get; private set; } = vec3.Zero;

			public void Average(vec3 v)
			{
				count++;

				// See https://stackoverflow.com/a/16757630.
				Value -= Value / count;
				Value += v / count;
			}

			public void Reset()
			{
				Value = vec3.Zero;
				count = 0;
			}
		}
	}
}
