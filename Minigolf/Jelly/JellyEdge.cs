﻿using Engine.Utility;

namespace Minigolf.Jelly
{
	public class JellyEdge
	{
		public JellyEdge(JellySpring spring1, JellySpring spring2)
		{
			Spring1 = spring1;
			Spring2 = spring2;
			Target = Utilities.Distance(spring1.Position, spring2.Position);
		}

		public JellySpring Spring1 { get; }
		public JellySpring Spring2 { get; }

		public float Target { get; }
	}
}
