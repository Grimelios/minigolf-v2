﻿using System.Diagnostics;
using Engine;
using Engine.Interfaces;
using GlmSharp;

namespace Minigolf.Jelly
{
	[DebuggerDisplay("Position={Position}")]
	public class JellySpring : IDynamic
	{
		public JellySpring(vec3 target, float mass, int index)
		{
			Target = target;
			Position = target;
			OldPosition = target;
			Mass = mass;
			Index = index;
		}

		public vec3 Position { get; set; }
		public vec3 OldPosition { get; set; }
		public vec3 Target { get; set; }

		// This is the vector used to apply spring physics *before* that resolution is actually applied. Useful for
		// processing golfball collisions against jelly.
		public vec3 Pushback { get; set; }

		// Mass is estimated by raycasting into the mesh based on vertex normal. The deeper the ray penetrates before
		// breaking out the other side, the greater the spring's mass.
		public float Mass { get; }
		public int Index { get; }

		public bool IsFixed { get; set; }
		public bool IsManual { get; set; }

		public void Apply(vec3 impulse)
		{
			if (IsFixed)
			{
				return;
			}

			Position += impulse / Mass;
		}

		public vec3 Precompute(vec3 p)
		{
			var result = vec3.Zero;
			result += (Target - p) * JellyConstants.KTarget;
			result.y -= JellyConstants.Gravity * Game.DeltaTime;
			result += (p - OldPosition) * JellyConstants.Damping;

			return result;
		}

		public void Update()
		{
			return;

			if (IsFixed)
			{
				return;
			}

			var p = Position;
			var temp = p;

			p += (Target - p) * JellyConstants.KTarget;
			p.y -= JellyConstants.Gravity * Game.DeltaTime;
			p += (p - OldPosition) * JellyConstants.Damping;
			Position = p;
			OldPosition = temp;

			//Velocity += (Target - Position) * JellyTest.KTarget;
			//Position += Velocity * Game.DeltaTime;
			//Velocity *= JellyTest.Damping;
		}
	}
}
