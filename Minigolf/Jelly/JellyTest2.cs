﻿using Engine;
using Engine.Core;
using Engine.Graphics._2D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Shapes._2D;
using GlmSharp;
using static Engine.GLFW;

namespace Minigolf.Jelly
{
	public class JellyTest2
	{
		private const float T = 0.5f;

		private vec2 p;
		private vec2 p1;
		private vec2 p2;
		private vec2 a1;
		private vec2 a2;

		private float mass;
		private float m1;
		private float m2;
		private float radius;

		private bool shouldDeform;

		public JellyTest2()
		{
			var center = (vec2)Resolution.WindowDimensions / 2;

			mass = 1;
			m1 = 1;
			m2 = 0.1f;
			p1 = center - new vec2(200, 0);
			p2 = center + new vec2(200, 0);
			p = center - new vec2(0, 50);
			radius = 20;

			InputProcessor.Add(data =>
			{
				const float Speed = 150;

				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				var up = keyboard.Query(GLFW_KEY_W, InputStates.Held);
				var down = keyboard.Query(GLFW_KEY_S, InputStates.Held);
				var left = keyboard.Query(GLFW_KEY_A, InputStates.Held);
				var right = keyboard.Query(GLFW_KEY_D, InputStates.Held);

				if (up ^ down)
				{
					p.y += Speed * (up ? -1 : 1) * Game.DeltaTime;
				}

				if (left ^ right)
				{
					p.x += Speed * (left ? -1 : 1) * Game.DeltaTime;
				}
			});
		}

		public void Update()
		{
			var impact = p1 + (p2 - p1) * T;
			var dY = p.y + radius - (p1.y + p2.y) / 2;

			shouldDeform = false;

			if (dY <= 0)
			{
				return;
			}

			var m = m1 + (m2 - m1) * T;
			var v = p + new vec2(0, radius) - impact;
			var f = mass * v;
			var r1 = p1 + f / m1;
			var r2 = p2 + f / m2;
			
			a1 = r1;
			a2 = r2;

			shouldDeform = true;
		}

		public void Draw(SpriteBatch sb)
		{
			sb.DrawLine(p1, p2, new Color(50));
			sb.Draw(new Circle(p, radius), 32, Color.Cyan);

			if (shouldDeform)
			{
				sb.DrawLine(p1 + (p2 - p1) * T, p + new vec2(0, radius), new Color(50));
				sb.DrawLine(a1, a2, Color.White);
			}
		}
	}
}
