﻿using Engine.Interfaces;
using Engine.Props;

namespace Minigolf.Jelly
{
	public class JellyConstants
	{
		public static float Damping { get; private set; }
		public static float Gravity { get; private set; }
		public static float KTarget { get; private set; }
		public static float KNeighbor { get; private set; }

		static JellyConstants()
		{
			Properties.Access(new JellyInternal());
		}

		private class JellyInternal : IReloadable
		{
			public bool Reload(PropertyAccessor accessor, out string message)
			{
				var keys = new[]
				{
					"jelly.damping",
					"jelly.k.target",
					"jelly.k.neighbor"
				};

				if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
				{
					return false;
				}

				Damping = results["jelly.damping"];
				KTarget = results["jelly.k.target"];
				KNeighbor = results["jelly.k.neighbor"];

				if (!accessor.GetFloat("jelly.gravity", PropertyConstraints.NonNegative, out var g, out message, this))
				{
					return false;
				}

				Gravity = g;

				return true;
			}
		}
	}
}
