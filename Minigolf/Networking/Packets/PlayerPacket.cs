﻿using Engine.Networking;
using Lidgren.Network;
using Minigolf.Players;

namespace Minigolf.Networking.Packets
{
	public class PlayerPacket : Packet
	{
		private PlayerPacketFlags flags;
		
		public PlayerPacket() : this(null)
		{
		}

		public PlayerPacket(PlayerId id) : base((uint)PacketTypes.Player)
		{
			Id = id;
			flags = PlayerPacketFlags.None;
		}

		public PlayerPacketFlags Flags
		{
			get => flags;
			set => flags = value;
		}

		public PlayerId Id { get; set; }
		public PlayerId[] Ids { get; set; }

		public int Hole { get; set; }
		public int Strokes { get; set; }

		public override void Decode(NetIncomingMessage message)
		{
			flags = (PlayerPacketFlags)message.ReadByte();

			bool isForward = (flags & PlayerPacketFlags.Forward) > 0;
			bool isHole = (flags & PlayerPacketFlags.Hole) > 0;
			bool isId = (flags & PlayerPacketFlags.Id) > 0;
			bool isStroke = (flags & PlayerPacketFlags.Stroke) > 0;

			if (!isForward || isId)
			{
				Id = message.ReadPlayerId();
			}

			if (isForward)
			{
				Ids = new PlayerId[message.ReadByte()];

				for (int i = 0; i < Ids.Length; i++)
				{
					Ids[i] = message.ReadPlayerId();
				}
			}

			if (isHole)
			{
				Hole = message.ReadInt16();
			}
			
			if (isHole || isStroke)
			{
				Strokes = message.ReadInt16();
			}
		}

		public override void Encode(NetOutgoingMessage message)
		{
			// TODO: If additional flags are added, this might need to change to a short.
			message.Write((byte)flags);

			bool isForward = (flags & PlayerPacketFlags.Forward) > 0;
			bool isHole = (flags & PlayerPacketFlags.Hole) > 0;
			bool isId = (flags & PlayerPacketFlags.Id) > 0;
			bool isStroke = (flags & PlayerPacketFlags.Stroke) > 0;

			// The regular ID doesn't need to be written for forwarding packets (unless the joining tag needs to be
			// updated).
			if (!isForward || isId)
			{
				message.Write(Id);
			}

			if ((flags & PlayerPacketFlags.Forward) > 0)
			{
				message.Write((byte)Ids.Length);

				foreach (var id in Ids)
				{
					message.Write(id);
				}
			}

			if (isHole)
			{
				message.Write((short)Hole);
			}
			
			if (isHole || isStroke)
			{
				message.Write((short)Strokes);
			}
		}
	}
}
