﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Networking.Packets
{
	public enum PacketTypes
	{
		Ball,
		Chat,
		Player,
		Round
	}
}
