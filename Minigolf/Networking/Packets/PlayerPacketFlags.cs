﻿using System;

namespace Minigolf.Networking.Packets
{
	[Flags]
	public enum PlayerPacketFlags
	{
		Forward = 1<<0,
		Hole = 1<<1,
		Id = 1<<2,
		Invite = 1<<3,
		Join = 1<<4,
		Leave = 1<<5,
		None = 0,
		Stroke = 1<<6
	}
}
