﻿using Engine.Networking;
using GlmSharp;
using Lidgren.Network;
using Minigolf.Players;

namespace Minigolf.Networking.Packets
{
	public class BallPacket : Packet
	{
		public BallPacket() : base((uint)PacketTypes.Ball)
		{
		}

		public BallPacket(PlayerId id, vec3 position, vec3 linearVelocity, vec3 angularVelocity, quat orientation) :
			base((uint)PacketTypes.Ball)
		{
			PlayerId = id;
			Position = position;
			LinearVelocity = linearVelocity;
			AngularVelocity = angularVelocity;
			Orientation = orientation;
		}

		public PlayerId PlayerId { get; private set; }

		public vec3 Position { get; private set; }
		public vec3 LinearVelocity { get; private set; }
		public vec3 AngularVelocity { get; private set; }

		public quat Orientation { get; private set; }

		public override void Decode(NetIncomingMessage message)
		{
			PlayerId = message.ReadPlayerId();
			Position = message.ReadVec3();
			LinearVelocity = message.ReadVec3();
			AngularVelocity = message.ReadVec3();
			Orientation = message.ReadQuat();
		}

		public override void Encode(NetOutgoingMessage message)
		{
			message.Write(PlayerId);
			message.Write(Position);
			message.Write(LinearVelocity);
			message.Write(AngularVelocity);
			message.Write(Orientation);
		}
	}
}
