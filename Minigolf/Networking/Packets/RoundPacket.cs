﻿using Engine.Networking;
using Lidgren.Network;

namespace Minigolf.Networking.Packets
{
	public class RoundPacket : Packet
	{
		public RoundPacket() : base((uint)PacketTypes.Round)
		{
		}

		public RoundPacket(int hole, long time) : base((uint)PacketTypes.Round)
		{
			Hole = hole;
			Time = time;
		}

		public int Hole { get; private set; }
		public long Time { get; private set; }

		public override void Decode(NetIncomingMessage message)
		{
			Hole = message.ReadByte();
			Time = message.ReadInt64();
		}

		public override void Encode(NetOutgoingMessage message)
		{
			message.Write((byte)Hole);
			message.Write(Time);
		}
	}
}
