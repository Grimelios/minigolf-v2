﻿using Engine.Networking;
using Lidgren.Network;
using Minigolf.Players;

namespace Minigolf.Networking.Packets
{
	public class ChatPacket : Packet
	{
		public ChatPacket() : base((uint)PacketTypes.Chat)
		{
		}

		public ChatPacket(PlayerId id, string message) : base((uint)PacketTypes.Chat)
		{
			PlayerId = id;
			Message = message;
		}

		public PlayerId PlayerId { get; private set; }

		public string Message { get; private set; }

		public override void Decode(NetIncomingMessage message)
		{
			PlayerId = message.ReadPlayerId();
			Message = message.ReadString();
		}

		public override void Encode(NetOutgoingMessage message)
		{
			message.Write(PlayerId);
			message.Write(Message);
		}
	}
}
