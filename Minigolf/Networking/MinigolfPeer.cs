﻿using System.Diagnostics;
using Engine.Networking;
using Lidgren.Network;
using Minigolf.Networking.Packets;
using Minigolf.Players;

namespace Minigolf.Networking
{
	public class MinigolfPeer : GamePeer
	{
		public MinigolfPeer(int port) : base(port)
		{
		}

		public PlayerCoordinator Coordinator { get; set; }

		public void Connect(string host, int port, PlayerId id)
		{
			var packet = new PlayerPacket(id);
			packet.Flags |= PlayerPacketFlags.Join;

			Connect(host, port, packet);
		}

		protected override Packet CreateDataPacket(int packetType)
		{
			switch ((PacketTypes)packetType)
			{
				case PacketTypes.Ball: return new BallPacket();
				case PacketTypes.Chat: return new ChatPacket();
				case PacketTypes.Player: return new PlayerPacket();
				case PacketTypes.Round: return new RoundPacket();
			}

			Debug.Fail($"Packet type {packetType} missing from switch.");

			return null;
		}

		protected override void OnStatusChange(NetIncomingMessage message)
		{
			var connection = message.SenderConnection;

			switch (connection.Status)
			{
				// This is called when a connection is approved.
				case NetConnectionStatus.Connected:
					var packet = DecodeHail(connection);

					if (packet != null)
					{
						ForwardToTargets(packet);
					}

					break;

				case NetConnectionStatus.Disconnected:
					break;

				// This is called when an outgoing connection is initiated.
				case NetConnectionStatus.InitiatedConnect:
					break;

				// This is called when an incoming connection is received (awaiting approval).
				case NetConnectionStatus.RespondedAwaitingApproval:
					ForwardToTargets(DecodeHail(connection));

					var hail = CreateMessage(Coordinator.OutgoingHail, true);
					connection.Approve(hail);

					break;

				// This is called when an incoming connection is approved (i.e. I just approved the connection).
				case NetConnectionStatus.RespondedConnect:
					break;
			}
		}

		private Packet DecodeHail(NetConnection connection)
		{
			var hail = connection.RemoteHailMessage;

			// For some reason, after a machine approves a connection, it seems to receives the same remote hail again
			// (but with the internal position already at the end due to previously being decoded). This duplicate hail
			// can be ignored.
			if (hail == null || hail.Position > 0)
			{
				return null;
			}

			var packet = new PlayerPacket();
			packet.Connection = connection;
			packet.Decode(hail);

			return packet;
		}
	}
}
