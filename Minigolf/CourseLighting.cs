﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Core;
using GlmSharp;

namespace Minigolf
{
	public class CourseLighting
	{
		public CourseLighting(float ambient, vec3 direction, Color color)
		{
			Ambient = ambient;
			Direction = direction;
			Color = color;
		}

		public float Ambient { get; }
		public vec3 Direction { get; }
		public Color Color { get; }
	}
}
