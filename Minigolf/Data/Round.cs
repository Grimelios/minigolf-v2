﻿using System;
using System.Collections.Generic;
using Engine.Interfaces;
using Engine.Networking;
using Engine.Props;
using Engine.Timing;
using Lidgren.Network;
using Minigolf.Networking;
using Minigolf.Networking.Packets;

namespace Minigolf.Data
{
	public class Round : IDynamic, IReloadable, INetworked
	{
		private Hole[] holes;

		private int holeIndex;

		// When the round is advanced, a time is coordinated among all connected players, such that the hole actually
		// advances (in theory) at exactly the same time.
		private SingleTimer timer;
		private MinigolfPeer peer;

		private float advanceTime;

		public Round(MinigolfPeer peer, int timeLimit, int strokeLimit)
		{
			this.peer = peer;

			TimeLimit = timeLimit;
			StrokeLimit = strokeLimit;
			timer = new SingleTimer(Advance);

			peer.Subscribe((uint)PacketTypes.Round, this);

			Properties.Access(this);
		}

		public event Action RoundStartEvent;
		public event Action RoundEndEvent;
		public event Action<Hole> HoleAdvanceEvent;

		public List<NetworkHandle> NetworkHandles { get; set; }

		public int TimeLimit { get; set; }
		public int StrokeLimit { get; set; }

		public Hole CurrentHole => holes[holeIndex];

		public bool HasStarted { get; private set; }

		public bool Reload(PropertyAccessor accessor, out string message)
		{
			return accessor.GetFloat("hole.advance.time", PropertyConstraints.Positive, out message, ref advanceTime,
				this);
		}

		public void Start(Hole[] holes)
		{
			this.holes = holes;

			HasStarted = true;
			RoundStartEvent?.Invoke();
			HoleAdvanceEvent?.Invoke(holes[0]);
		}

		public void End()
		{
			RoundEndEvent?.Invoke();
			HasStarted = false;
		}

		public void Dispose()
		{
			peer.Unsubscribe(this);
		}

		// Note that this function is only called from the leader.
		public void OnHoleFinished()
		{
			timer.Duration = advanceTime;
			timer.IsPaused = false;

			var time = DateTime.Now.Ticks + (long)(advanceTime * TimeSpan.TicksPerSecond);

			peer.Send(new RoundPacket(holeIndex, time), NetDeliveryMethod.ReliableUnordered);
		}

		public void Process(Packet packet)
		{
			Process((RoundPacket)packet);
		}

		private void Process(RoundPacket packet)
		{
			// TODO: Account for the target time being in the past.
			var duration = (float)(packet.Time - DateTime.Now.Ticks) / TimeSpan.TicksPerSecond;

			timer.Duration = duration;
			timer.IsPaused = false;
		}

		private void Advance(float t)
		{
			holes[holeIndex++].IsActive = false;

			if (holeIndex == holes.Length)
			{
				End();
			}
			else
			{
				var hole = holes[holeIndex];
				hole.IsActive = true;

				HoleAdvanceEvent?.Invoke(hole);
			}
		}

		public void Update()
		{
			timer.Update();
		}
	}
}
