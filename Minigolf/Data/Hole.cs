﻿using System.Linq;
using Engine.Sensors;
using Engine.Shapes._3D;
using GlmSharp;

namespace Minigolf.Data
{
	public class Hole
	{
		private Shape3D[] bounds;

		public Hole(string name, int par, int index, vec3 spawn, Box hole, Shape3D[] bounds)
		{
			this.bounds = bounds;

			Name = name;
			Par = par;
			Index = index;
			Spawn = spawn;
			HoleSensor = new Sensor(SensorTypes.Zone, this, SensorGroups.Hole, SensorGroups.None, hole);
		}

		public string Name { get; }
		public int Par { get; }
		public int Index { get; }
		public vec3 Spawn { get; }
		public Sensor HoleSensor { get; }

		// This means that the hole is currently being played.
		public bool IsActive { get; set; }

		public bool IsInBounds(vec3 p)
		{
			return bounds.Any(b => b.Contains(p));
		}
	}
}
