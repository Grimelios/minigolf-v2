﻿using System;
using Engine.Core;
using Engine.Core._3D;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Physics;
using Engine.Shapes._3D;
using Engine.Utility;
using GlmSharp;
using Jitter.Dynamics;
using Minigolf.Physics;
using Newtonsoft.Json.Linq;

namespace Minigolf.Data
{
	public class Course : IDisposable
	{
		private Scene scene;
		private Model model;
		private Hole[] holes;

		public Course(Scene scene)
		{
			this.scene = scene;
		}

		public string Name { get; private set; }

		public CourseLighting Lighting { get; private set; }

		public Hole[] Load(string filename)
		{
			var json = JsonUtilities.Load("Courses/" + filename);

			model = new Model(json.Value<string>("Mesh"));
			scene.Renderer.Add(model);

			var shape = TriangleMeshLoader.Load(model.Mesh);
			var body = new RigidBody(shape, PhysicsGroups.Environment, RigidBodyTypes.Static);

			//scene.World.AddBody(body);

			var light = json["Lighting"];
			var ambient = light.Value<float>("Ambient");

			// The light direction should already be normalized in the file, but normalizing it here anyway is safer.
			var direction = Utilities.Normalize(Utilities.ParseVec3(light.Value<string>("Direction")));
			var color = Color.Parse(light.Value<string>("Color"));

			Lighting = new CourseLighting(ambient, direction, color);

			var array = (JArray)json["Holes"];

			Name = json.Value<string>("Name");
			holes = new Hole[array.Count];

			for (int i = 0; i < holes.Length; i++)
			{
				var token = array[i];
				var name = token.Value<string>("Name");
				var par = token.Value<int>("Par");
				var spawn = Utilities.ParseVec3(token.Value<string>("Spawn"));
				var hole = ParseHole(token.Value<string>("Hole"));
				var shapes = (JArray)token["Bounds"];
				var bounds = new Shape3D[shapes.Count];

				for (int j = 0; j < bounds.Length; j++)
				{
					bounds[j] = ParseShape(shapes[j]);
				}

				holes[i] = new Hole(name, par, i, spawn, hole, bounds);
			}

			foreach (var hole in holes)
			{
				scene.Space.Add(hole.HoleSensor);
			}

			holes[0].IsActive = true;

			return holes;
		}
		
		public Mesh Mesh => model.Mesh;

		private Box ParseHole(string s)
		{
			var tokens = s.Split('|');
			var x = float.Parse(tokens[0]);
			var y = float.Parse(tokens[1]);
			var z = float.Parse(tokens[2]);
			var width = float.Parse(tokens[3]);
			var height = float.Parse(tokens[4]);
			var depth = float.Parse(tokens[5]);
			var box = new Box(width, height, depth);
			box.Position = new vec3(x, y, z);

			return box;
		}

		private Shape3D ParseShape(JToken token)
		{
			var type = (ShapeTypes3D)Enum.Parse(typeof(ShapeTypes3D), token.Value<string>("Type"));
			var pToken = token["Position"];
			var qToken = token["Orientation"];

			var p = pToken != null ? Utilities.ParseVec3(pToken.Value<string>()) : vec3.Zero;
			var q = qToken != null ? Utilities.ParseQuat(qToken.Value<string>()) : quat.Identity;

			Shape3D shape = null;

			switch (type)
			{
				case ShapeTypes3D.Box:
					var width = token.Value<float>("Width");
					var height = token.Value<float>("Height");
					var depth = token.Value<float>("Depth");

					shape = new Box(width, height, depth);

					break;

				case ShapeTypes3D.Cylinder:
					var radius = token.Value<float>("Radius");
					height = token.Value<float>("Height");

					shape = new Cylinder(height, radius);

					break;
			}

			shape.Position = p;
			shape.Orientation = q;
			shape.IsOrientable = q != quat.Identity;

			return shape;
		}

		public void Dispose()
		{
			scene.Renderer.Remove(model);

			foreach (var hole in holes)
			{
				scene.Space.Remove(hole.HoleSensor);
			}
		}
	}
}
