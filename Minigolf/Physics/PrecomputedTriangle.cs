﻿using Engine.Physics;
using Engine.Utility;
using GlmSharp;

namespace Minigolf.Physics
{
	// See https://www.geometrictools.com/Documentation/IntersectionMovingSphereTriangle.pdf. This class is used for
	// computing swept-sphere-triangle collisions.
	public class PrecomputedTriangle
	{
		public PrecomputedTriangle(vec3[] points, WindingTypes winding) :
			this(points, Utilities.ComputeNormal(points, winding))
		{
		}

		public PrecomputedTriangle(vec3[] points, vec3 normal)
		{
			Points = points;
			Normal = normal;
			Edges = new []
			{
				points[1] - points[0],
				points[2] - points[1],
				points[0] - points[2]
			};

			EdgeLengthsSquared = new []
			{
				Utilities.LengthSquared(Edges[0]),
				Utilities.LengthSquared(Edges[1]),
				Utilities.LengthSquared(Edges[2])
			};

			EdgesCrossNormal = new []
			{
				Utilities.Cross(Edges[0], Normal),
				Utilities.Cross(Edges[1], Normal),
				Utilities.Cross(Edges[2], Normal)
			};
		}

		public vec3[] Points { get; }
		public vec3[] Edges { get; }
		public vec3[] EdgesCrossNormal { get; }
		public vec3 Normal { get; }

		public float[] EdgeLengthsSquared { get; }
	}
}
