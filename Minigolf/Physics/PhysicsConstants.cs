﻿using Engine.Props;

namespace Minigolf.Physics
{
	public static class PhysicsConstants
	{
		static PhysicsConstants()
		{
			var accessor = Properties.Access();

			Gravity = accessor.GetFloat("gravity");
		}

		public static float Gravity { get; }
	}
}
