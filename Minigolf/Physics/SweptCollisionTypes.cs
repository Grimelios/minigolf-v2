﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Physics
{
	public enum SweptCollisionTypes
	{
		Edge,
		Embedded,
		None,
		Point,
		Surface
	}
}
