﻿using System;
using System.Linq;
using Engine;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;

namespace Minigolf.Physics
{
	public class SurfaceTriangle
	{
		/*
		public static float ComputeForgiveness(vec3 p, SurfaceTriangle surface)
		{
			return ComputeForgiveness(p, surface.Normal, surface.Points);
		}

		public static float ComputeForgiveness(vec3 p, vec3 n, vec3[] triangle)
		{
			// To compute the shortest distance to an edge of the triangle, points are rotated to a flat plane first
			// (using the surface normal).
			var q = Utilities.Orientation(n, vec3.UnitY);
			var flatP = (q * p).swizzle.xz;
			var flatPoints = triangle.Select(v => (q * v).swizzle.xz).ToArray();
			var d = float.MaxValue;

			for (int i = 0; i < flatPoints.Length; i++)
			{
				var p1 = flatPoints[i];
				var p2 = flatPoints[(i + 1) % 3];

				d = Math.Min(d, Utilities.DistanceToLine(flatP, p1, p2, out _));
			}

			return d;
		}
		*/

		// This is used to project points onto the triangle.
		private quat flatProjection;

		// "f" stands for "flat".
		private vec2 fp0;
		private vec2 fp1;
		private vec2 fp2;

		// For projection purposes, the double area (2 * area) is what you want.
		private float doubleArea;

		public SurfaceTriangle(vec3 p0, vec3 p1, vec3 p2, WindingTypes winding, int material, int index) :
			this(p0, p1, p2, Utilities.ComputeNormal(p0, p1, p2, winding), material, index)
		{
		}

		public SurfaceTriangle(vec3[] points, vec3 normal, int material, int index) :
			this(points[0], points[1], points[2], normal, material, index)
		{
		}

		private SurfaceTriangle(vec3 p0, vec3 p1, vec3 p2, vec3 normal, int material, int index)
		{
			Points = new[] { p0, p1, p2 };
			Normal = normal;
			Index = index;
			Material = material;
			flatProjection = Utilities.ComputeFlatProjection(normal);

			var flatPoints = new vec2[3];

			for (int i = 0; i < Points.Length; i++)
			{
				flatPoints[i] = (Points[i] * flatProjection).swizzle.xz;
			}

			fp0 = flatPoints[0];
			fp1 = flatPoints[1];
			fp2 = flatPoints[2];

			// See https://stackoverflow.com/a/14382692/7281613.
			doubleArea = -fp1.y * fp2.x + fp0.y * (-fp1.x + fp2.x) + fp0.x * (fp1.y - fp2.y) + fp1.x * fp2.y;
		}

		public vec3[] Points { get; }
		public vec3 Normal { get; }

		// This is the index of the triangle within the mesh (defaults to -1 if the surface isn't constructed from a
		// mesh).
		public int Index { get; }
		public int Material { get; }

		public bool Project(vec3 p, out vec3 result)
		{
			var a = (p * flatProjection).swizzle.xz;

			// TODO: This can be made more efficient (see the link below, then the comments below the answer).
			// See https://stackoverflow.com/a/14382692/7281613.
			float s = 1 / doubleArea * (fp0.y * fp2.x - fp0.x * fp2.y + a.x * (fp2.y - fp0.y) + a.y * (fp0.x - fp2.x));
			float t = 1 / doubleArea * (fp0.x * fp1.y - fp0.y * fp1.x + a.x * (fp0.y - fp1.y) + a.y * (fp1.x - fp0.x));
			float u = 1 - s - t;

			// The projected point is computed regardless (to help account for weird behavior on the edges of
			// triangles, where an actor could theoretically hit a seam and give a false negative). In those cases, the
			// actor continues running past the triangle (which shouldn't be noticeable in practice, since this
			// situation can only occur when you're extremely close to an edge).
			result = Points[1] * s + Points[2] * t + Points[0] * u;

			return s >= 0 && t >= 0 && u >= 0;
		}

		// TODO: If indexes are used to determine same-ness, this function could be removed.
		public bool HasEdge(Edge edge)
		{
			for (int i = 0; i < 3; i++)
			{
				if (edge.IsSame(Points[i], Points[(i + 1) % 3]))
				{
					return true;
				}
			}

			return false;
		}

		// TODO: If indexes are used to determine same-ness, this function could be removed (same as above).
		public bool HasVertex(vec3 p)
		{
			return Points.Any(point => Utilities.DistanceSquared(point, p) < Constants.DefaultEpsilon);
		}

		public float ComputeForgiveness(vec3 p)
		{
			// To compute the shortest distance to an edge of the triangle, points are rotated to a flat plane first
			// (using the surface normal).
			var q = Utilities.Orientation(Normal, vec3.UnitY);
			var flatP = (q * p).swizzle.xz;
			var flatPoints = Points.Select(v => (q * v).swizzle.xz).ToArray();
			var d = float.MaxValue;

			for (int i = 0; i < flatPoints.Length; i++)
			{
				var p1 = flatPoints[i];
				var p2 = flatPoints[(i + 1) % 3];

				d = Math.Min(d, Utilities.DistanceToLine(flatP, p1, p2, out _));
			}

			return d;
		}
	}
}
