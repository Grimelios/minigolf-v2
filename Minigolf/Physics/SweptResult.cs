﻿using Engine.Utility;
using GlmSharp;

namespace Minigolf.Physics
{
	public class SweptResult
	{
		private float edgeT;

		public SweptResult(SweptCollisionTypes type, PrecomputedTriangle triangle, vec3 p)
		{
			Type = type;

			var points = triangle.Points;

			// TODO: As an optimization, try to determine closest edge/point from the sweep code itself.
			switch (type)
			{
				case SweptCollisionTypes.Surface:
					Triangle = triangle;

					break;

				case SweptCollisionTypes.Edge:
					var min = float.MaxValue;
					var index = -1;

					for (int i = 0; i < 3; i++)
					{
						var p0 = points[i];
						var p1 = points[(i + 1) % 3];
						var d = Utilities.DistanceSquaredToLine(p, p0, p1, out var eT);

						if (d < min)
						{
							min = d;
							index = i;
							edgeT = eT;
						}
					}

					Edge = new Edge(points[index], points[(index + 1) % 3]);

					break;

				case SweptCollisionTypes.Point:
					min = float.MaxValue;
					index = -1;

					for (int i = 0; i < 3; i++)
					{
						var d = Utilities.DistanceSquared(p, points[i]);

						if (d < min)
						{
							min = d;
							index = i;
						}
					}

					Point = points[index];

					break;
			}
		}

		public SweptCollisionTypes Type { get; }
		public PrecomputedTriangle Triangle { get; }
		public Edge Edge { get; }
		public vec3 Point { get; }

		// TODO: Should probably be optimized using indexes.
		public bool IsDuplicate(SweptResult result)
		{
			const float Epsilon = 0.001f;

			if (Type != result.Type)
			{
				return false;
			}

			switch (Type)
			{
				case SweptCollisionTypes.Edge:
					return Edge.IsSame(result.Edge);

				case SweptCollisionTypes.Point:
					return Utilities.DistanceSquared(Point, result.Point) < Epsilon;
			}

			return false;
		}

		public vec3 ComputeNormal(vec3 p)
		{
			switch (Type)
			{
				case SweptCollisionTypes.Surface:
					return Triangle.Normal;

				case SweptCollisionTypes.Edge:
					var e = Edge.P1 + (Edge.P2 - Edge.P1) * edgeT;

					return Utilities.Normalize(p - e);

				case SweptCollisionTypes.Point:
					return Utilities.Normalize(p - Point);
			}

			return vec3.Zero;
		}
	}
}
