﻿using System.Diagnostics;
using Engine.Utility;
using GlmSharp;

namespace Minigolf.Physics
{
	[DebuggerDisplay("P1={P1}, P2={P2}")]
	public class Edge
	{
		public Edge(vec3 p1, vec3 p2)
		{
			P1 = p1;
			P2 = p2;
			Direction = Utilities.Normalize(p2 - p1);
		}

		public vec3 P1 { get; }
		public vec3 P2 { get; }
		public vec3 Direction { get; }

		public bool IsSame(Edge other)
		{
			return IsSame(other.P1, other.P2);
		}

		public bool IsSame(vec3 e1, vec3 e2)
		{
			const float Epsilon = 0.001f;

			// Even for matching edges, the order of points won't always be the same.
			return (Utilities.DistanceSquared(P1, e1) < Epsilon && Utilities.DistanceSquared(P2, e2) < Epsilon) ||
				(Utilities.DistanceSquared(P1, e2) < Epsilon && Utilities.DistanceSquared(P2, e1) < Epsilon);
		}
	}
}
