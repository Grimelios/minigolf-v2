﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Graphics._3D;
using Engine.Physics;
using Engine.Utility;
using GlmSharp;

namespace Minigolf.Physics
{
	public class PhysicsHelper
	{
		public static bool SweepSphere(vec3 sP, vec3 sV, float sR, Mesh mesh, out float t,
			out List<SweptResult> results)
		{
			const float Epsilon = 0.001f;

			t = 0;
			results = new List<SweptResult>();

			var points = mesh.Points;
			var vertices = mesh.Vertices;
			var indexes = mesh.Indices;
			var tMin = -1f;

			// TODO: Optimize (by not casting against every single triangle each frame, probably using an octree).
			for (int i = 0; i < indexes.Length; i += 3)
			{
				var p0 = points[vertices[indexes[i]].x];
				var p1 = points[vertices[indexes[i + 1]].x];
				var p2 = points[vertices[indexes[i + 2]].x];

				// TODO: Precompute triangles for the entire mesh when the course loads (or even consider baking the data into the map file itself to reduce loading times => Performance should be measured).
				var precomputed = new PrecomputedTriangle(new [] { p2, p1, p0 }, WindingTypes.CounterClockwise);

				if (SweepSphere(sP, sV, sR, precomputed, vec3.Zero, out t, out var p, out var type))
				{
					// An epsilon value is applied such that very close collisions are counted as occurring at the same
					// time.
					var shouldRecord = tMin < 0 || t <= tMin + Epsilon;

					if (!shouldRecord)
					{
						continue;
					}

					var shouldClear = tMin >= 0 && t < tMin - Epsilon;

					if (shouldClear)
					{
						results.Clear();
					}

					tMin = t;

					// TODO: Duplicate checks could be optimized using triangle indexes (rather than constructing a new result object).
					var result = new SweptResult(type, precomputed, p);

					// Points and edges can be shared among multiple triangles (which can result in duplicate
					// contacts).
					if (type == SweptCollisionTypes.Surface || !results.Any(r => r.IsDuplicate(result)))
					{
						results.Add(result);
					}
				}
			}

			if (tMin >= 0)
			{
				t = tMin;

				return true;
			}

			return false;
		}

		public static bool SweepSphere(vec3 sP, vec3 sV, float sR, PrecomputedTriangle triangle, vec3 tV, out float t,
			out SweptResult result)
		{
			if (SweepSphere(sP, sV, sR, triangle, tV, out t, out var r, out var type))
			{
				result = new SweptResult(type, triangle, r);

				return true;
			}

			result = null;

			return false;
		}

		public static bool SweepSphere(vec3 sP, vec3 sV, float sR, PrecomputedTriangle triangle, vec3 tV, out float t,
			out vec3 result, out SweptCollisionTypes type)
		{
			t = 0;
			type = SweptCollisionTypes.None;

			// See https://www.geometrictools.com/Documentation/IntersectionMovingSphereTriangle.pdf.
			var points = triangle.Points;
			var dSquared = Utilities.DistanceSquaredToTriangle(sP, points, out result);
			var rSquared = sR * sR;

			// This means that the sphere is already embedded in the triangle. Note that the epsilon helps prevent
			// false positives when a golf ball hits a triangle, snaps its position to touch, and bounces away.
			if (dSquared <= rSquared - 0.001f)
			{
				type = SweptCollisionTypes.Embedded;

				return true;
			}

			var v = sV - tV;
			var sqrV = Utilities.LengthSquared(v);

			// If the sphere isn't moving (and not already touching), it definitely won't intersect.
			if (sqrV == 0)
			{
				return false;
			}

			var deltas = new vec3[3];

			for (int i = 0; i < 3; i++)
			{
				deltas[i] = sP - points[i];
			}

			var n = triangle.Normal;
			var eXn = triangle.EdgesCrossNormal;
			var nDotDelta0 = Utilities.Dot(n, deltas[0]);
			var nvDot = Utilities.Dot(n, v);

			if (nDotDelta0 >= sR)
			{
				// This means that the sphere is moving away from (or parallel to) the triangle plane.
				if (nvDot >= 0)
				{
					return false;
				}

				var tBar = (sR - nDotDelta0) / nvDot;

				if (tBar <= 1 && Utilities.Dot(eXn[0], deltas[0]) + Utilities.Dot(eXn[0], v) * tBar > 0 &&
					Utilities.Dot(eXn[1], deltas[1]) + Utilities.Dot(eXn[1], v) * tBar > 0 &&
					Utilities.Dot(eXn[2], deltas[2]) + Utilities.Dot(eXn[2], v) * tBar > 0)
				{
					t = tBar;
					type = SweptCollisionTypes.Surface;
					result = sP + v * t;

					return true;
				}
			}
			else if (nDotDelta0 <= -sR)
			{
				// Same note as above (the sphere is moving away from, or parallel to, the triangle plane).
				if (nvDot <= 0)
				{
					return false;
				}

				var tBar = (-sR - nDotDelta0) / nvDot;

				if (tBar <= 1 && Utilities.Dot(eXn[0], deltas[0]) + Utilities.Dot(eXn[0], v) * tBar > 0 &&
					Utilities.Dot(eXn[1], deltas[1]) + Utilities.Dot(eXn[1], v) * tBar > 0 &&
					Utilities.Dot(eXn[2], deltas[2]) + Utilities.Dot(eXn[2], v) * tBar > 0)
				{
					t = tBar;
					type = SweptCollisionTypes.Surface;
					result = sP + v * t;

					return true;
				}
			}

			// Some of these variable names kinda suck, but they're copied from the reference PDF linked above.
			var del = new float[3];
			var delp = new float[3];
			var nu = new float[3];
			var e = triangle.Edges;
			var sqrE = triangle.EdgeLengthsSquared;

			for (int im1 = 2, i = 0; i < 3; im1 = i++)
			{
				del[i] = Utilities.Dot(e[i], deltas[i]);
				delp[im1] = Utilities.Dot(e[im1], deltas[i]);
				nu[i] = Utilities.Dot(e[i], v);
			}

			for (int i = 2, ip1 = 0; ip1 < 3; i = ip1++)
			{
				var hatV = v - e[i] * (nu[i] / sqrE[i]);
				var sqrHatV = Utilities.LengthSquared(hatV);

				if (sqrHatV <= 0)
				{
					continue;
				}

				var hatDelta = deltas[i] - e[i] * del[i] / sqrE[i];
				var alpha = -Utilities.Dot(hatV, hatDelta);

				if (alpha < 0)
				{
					continue;
				}
				
				var sqrHatDelta = Utilities.LengthSquared(hatDelta);
				var beta = alpha * alpha - sqrHatV * (sqrHatDelta - rSquared);

				if (beta < 0)
				{
					continue;
				}

				var tBar = (alpha - (float)Math.Sqrt(beta)) / sqrHatV;
				var mu = Utilities.Dot(eXn[i], deltas[i]);
				var omega = Utilities.Dot(eXn[i], hatV);

				if (tBar <= 1 && mu + omega * tBar < 0 && del[i] + nu[i] * tBar >= 0 && delp[i] + nu[i] * tBar <= 0)
				{
					t = tBar;
					type = SweptCollisionTypes.Edge;
					result = sP + v * t;

					return true;
				}
			}

			for (int im1 = 2, i = 0; i < 3; im1 = i++)
			{
				var alpha = -Utilities.Dot(v, deltas[i]);

				if (alpha < 0)
				{
					continue;
				}

				var sqrDelta = Utilities.LengthSquared(deltas[i]);
				var beta = alpha * alpha - sqrV * (sqrDelta - rSquared);

				if (beta < 0)
				{
					continue;
				}

				var tBar = (alpha - (float)Math.Sqrt(beta)) / sqrV;

				if (tBar <= 1 && delp[im1] + nu[im1] * tBar >= 0 && del[i] + nu[i] * tBar <= 0)
				{
					t = tBar;
					type = SweptCollisionTypes.Point;
					result = sP + v * t;

					return true;
				}
			}

			return false;
		}
	}
}
