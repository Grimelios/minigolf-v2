﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minigolf.Physics
{
	public static class PhysicsGroups
	{
		public const uint Environment = 1<<0;
		public const uint Golfball = 1<<1;
	}
}
