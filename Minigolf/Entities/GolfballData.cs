﻿using System;
using Engine.Interfaces;
using Engine.Props;

namespace Minigolf.Entities
{
	public class GolfballData : IReloadable
	{
		public GolfballData()
		{
			Properties.Access(this);
		}

		public float AerialAngularDeceleration { get; private set; }
		public float EdgeForgiveness { get; private set; }
		public float MaxBoost { get; private set; }

		public bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"golfball.aerial.angular.deceleration",
				"golfball.edge.forgiveness",
				"golfball.max.boost"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			AerialAngularDeceleration = results["golfball.aerial.angular.deceleration"];
			EdgeForgiveness = results["golfball.edge.forgiveness"];
			MaxBoost = results["golfball.max.boost"];

			return true;
		}
	}
}
