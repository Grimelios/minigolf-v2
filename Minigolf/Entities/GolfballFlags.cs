﻿using System;

namespace Minigolf.Entities
{
	[Flags]
	public enum GolfballFlags
	{
		IsAirborne = 1<<0,
		IsHoled = 1<<1,
		IsRespawnAvailable = 1<<2,
		IsStopped = 1<<3,

		None = 0
	}
}
