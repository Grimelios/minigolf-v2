﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Engine;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Physics;
using Engine.Props;
using Engine.Shapes._3D;
using Engine.UI;
using Engine.Utility;
using GlmSharp;
using Lidgren.Network;
using Minigolf.Data;
using Minigolf.Jelly;
using Minigolf.Networking.Packets;
using Minigolf.Physics;
using Minigolf.Players;
using Minigolf.UI.Hud;
using Newtonsoft.Json.Linq;
using static Engine.GLFW;

namespace Minigolf.Entities
{
	public class Golfball : Entity
	{
		private static GolfballData golfballData = new GolfballData();

		private Player player;
		private Putter putter;
		private Scoreboard scoreboard;
		private HoleResultDisplay resultDisplay;
		private Checkpoint checkpoint;
		private Mesh courseMesh;

		// This gravity vector is valuable for courses with Galaxy-style gravity zones. For regular maps, it always
		// points in one direction.
		private vec3 gravity;
		private vec3 linearVelocity;
		private vec3 angularVelocity;
		private vec3 eulerOrientation;

		private List<SurfaceTriangle> surfaces;
		private List<Edge> edges;
		private List<vec3> points;

		private List<JellySpring> jellySurfaces;
		private List<JellySpring> jellyEdges;
		private List<JellySpring> jellyPoints;

		private float mass;
		private float radius;
		private float bounciness;

		private GolfballFlags flags;

		// This is temporary for physics testing (the IsUpdateEnabled flag can't be used directly because the ball mesh
		// still needs to update each frame).
		private bool shouldUpdate;

		public Golfball(Player player, Putter putter, Mesh courseMesh) : base(EntityGroups.Golfball)
		{
			this.player = player;
			this.putter = putter;
			this.courseMesh = courseMesh;

			player.Golfball = this;
			gravity = new vec3(0, -PhysicsConstants.Gravity, 0);

			// TODO: Flags should be set to None when the ground raycast is restored.
			flags = GolfballFlags.IsAirborne;
			checkpoint = new Checkpoint(this);
			mass = 1;

			surfaces = new List<SurfaceTriangle>();
			edges = new List<Edge>();
			points = new List<vec3>();

			jellySurfaces = new List<JellySpring>();
			jellyEdges = new List<JellySpring>();
			jellyPoints = new List<JellySpring>();

			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW_KEY_G, InputStates.PressedThisFrame))
				{
					shouldUpdate = true;
				}

				if (keyboard.Query(GLFW_KEY_I, InputStates.PressedThisFrame))
				{
					linearVelocity = new vec3(1, 0, -0.01f);
				}

				if (keyboard.Query(GLFW_KEY_R, InputStates.PressedThisFrame))
				{
					position = checkpoint.Position;
					orientation = quat.Identity;
					linearVelocity = vec3.Zero;
					angularVelocity = vec3.Zero;

					surfaces.Clear();
					edges.Clear();
					points.Clear();

					flags = GolfballFlags.IsAirborne;
					shouldUpdate = false;
				}
			});
		}

		public float Bounciness
		{
			get => bounciness;
			set
			{
				Debug.Assert(value >= 0 && value <= 1, "Bounciness must be between zero and one (inclusive).");

				bounciness = value;
			}
		}

		public float Radius => radius;

		public vec3 LinearVelocity
		{
			get => linearVelocity;
			set => linearVelocity = value;
		}

		public vec3 AngularVelocity
		{
			get => angularVelocity;
			set => angularVelocity = value;
		}

		public Player Player => player;
		public JellyMesh JellyMesh { get; set; }

		public List<SurfaceTriangle> Surfaces => surfaces;

		public bool IsAirborne
		{
			get => (flags & GolfballFlags.IsAirborne) > 0;
			set
			{
				if (value)
				{
					flags |= GolfballFlags.IsAirborne;
				}
				else
				{
					flags &= ~GolfballFlags.IsAirborne;
				}
			}
		}

		public bool IsHoled
		{
			get => (flags & GolfballFlags.IsHoled) > 0;
			set
			{
				if (value)
				{
					flags |= GolfballFlags.IsHoled;
				}
				else
				{
					flags &= ~GolfballFlags.IsHoled;
				}
			}
		}

		public bool IsRespawnAvailable => (flags & GolfballFlags.IsRespawnAvailable) > 0;
		public bool IsStopped => (flags & GolfballFlags.IsStopped) > 0;

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "Golfball.obj", out var bounds);
			radius = bounds.x / 2;

			// Only local players directly notify the coordinator when the ball is holed. External players are instead
			// updated via network messages.
			if (player != null)
			{
				var sensor = CreateSensor(scene, new Sphere(radius), SensorGroups.Golfball, SensorGroups.Hole);
				sensor.OnSense = (type, owner) =>
				{
					if (!player.IsLocal)
					{
						return;
					}

					var hole = (Hole)owner;

					if (hole.IsActive)
					{
						OnHole(hole);
					}
					// If the player manages to sink the wrong hole, a custom message is displayed.
					else
					{
						resultDisplay.Refresh(-1, 0);
					}
				};
			}

			var canvas = scene.Canvas;
			scoreboard = canvas.GetElement<Scoreboard>();
			resultDisplay = canvas.GetElement<HoleResultDisplay>();

			base.Initialize(scene, data);
		}

		public void OnJellyCollision(JellySpring[] springs, SweptCollisionTypes type)
		{
			switch (type)
			{
				case SweptCollisionTypes.Surface:
					foreach (var s in springs)
					{
						jellySurfaces.Add(s);
					}

					break;

				case SweptCollisionTypes.Edge:
					jellyEdges.Add(springs[0]);
					jellyEdges.Add(springs[1]);

					break;

				case SweptCollisionTypes.Point:
					jellyEdges.Add(springs[0]);

					break;
			}
		}

		private void OnHole(Hole hole)
		{
			var strokes = putter.Strokes;

			scoreboard.Add(hole, strokes);
			resultDisplay.Refresh(strokes, hole.Par);
			flags |= GolfballFlags.IsHoled;
			flags &= ~GolfballFlags.IsRespawnAvailable;
			player.IsHoled = true;
		}

		public void Respawn(vec3 spawn)
		{
			const int Range = 1000;

			position = spawn;
			checkpoint.Refresh();

			return;

			// TODO: Account for potentially raycasting down to touch multiple contacts at once (could maybe be prevented in the editor).
			// This should never happen in the final game (especially once an editor is put in place to guarantee valid
			// spawn points), but it's still safer to keep the early return (and better during development).
			if (!courseMesh.Raycast(spawn, -vec3.UnitY, Range, out var results))
			{
				position = spawn;

				return;
			}

			var n = results.Normal;

			// TODO: Pull triangle index as well.
			surfaces.Add(new SurfaceTriangle(results.Triangle, n, results.Material, -1));
			flags = GolfballFlags.IsStopped;

			SetPosition(results.Position + n * radius, false);
			SetOrientation(quat.Identity, false);

			linearVelocity = vec3.Zero;
			angularVelocity = vec3.Zero;
			eulerOrientation = vec3.Zero;

			checkpoint.Refresh();
		}

		public void Hit(vec3 v)
		{
			// TODO: Consider mass when applying a force from the putter.
			linearVelocity = v;
			flags &= ~GolfballFlags.IsStopped;
			flags |= GolfballFlags.IsRespawnAvailable;
		}

		public void ResetToCheckpoint()
		{
			SetPosition(checkpoint.Position, false);
			SetOrientation(checkpoint.Orientation, false);

			linearVelocity = vec3.Zero;
			angularVelocity = vec3.Zero;
			eulerOrientation = vec3.Zero;

			flags = GolfballFlags.IsStopped;
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			return accessor.GetFloat("golfball.bounciness", PropertyConstraints.ZeroToOne, out message, ref bounciness,
				this);
		}

		/*
		public override bool OnCollision(vec3 p, vec3 normal, vec3[] triangle)
		{
			var v = controllingBody.LinearVelocity.ToVec3();

			if (!isAirborne)
			{
				var d = Utilities.Dot(v, normal);

				// TODO: Transition to the new ground triangle.
				if (d >= 0 || -d < golfballData.TransitionThreshold)
				{
					return false;
				}

				// When a grounded golfball collides with another triangle steep enough to cause an upward bounce (even
				// a small bounce), the ball is given a small vertical boost based on spin (i.e. a faster spin in the
				// direction of the wall gives a stronger boost). This boost is further reduced using a dot product,
				// such that head-on (i.e. perpendicular) collisions receive the strongest boost, while glancing blows
				// barely get anything.
				var vFlat = v.swizzle.xz;
				var flatLength = Utilities.Length(vFlat);

				// TODO: Consider squaring the dot product (i.e. apply exponentially, not linearly).
				var dot = Utilities.Dot(-vFlat / flatLength, Utilities.Normalize(normal.swizzle.xz));

				// TODO: This should really be based on angular velocity rather than linear.
				// The divisor here is arbitrary (it still linearly scales the upward boost based on incoming
				// velocity, just with lower resulting values).
				var correction = Math.Min(flatLength / 60 * dot, golfballData.MaxBoost);
				var nCorrected = Utilities.Normalize(normal + new vec3(0, correction, 0));
				v = Utilities.Reflect(v, nCorrected) * bounciness;

				// When a bounce occurs, angular velocity is artificially set to rotate in the new direction of
				// movement (relative to the flat XZ plane).
				controllingBody.AngularVelocity = new vec3(v.z, 0, -v.x).ToJVector() * Constants.Pi;

				if (v.y > golfballData.TakeoffThreshold)
				{
					BecomeAirborne();
				}
			}
			// TODO: This is temporary to enable aerial wall collisions.
			else if (normal.y == 0)
			{
				return true;
			}
			// TODO: Should really check the dot product between velocity and surface normal to determine landing.
			// A landing occurs (i.e. the golfball transitions from air to ground) when a downward collision occurs
			// with slow enough velocity (note that the "downward" direction can change based on gravity).
			else if (normal.y > 0 && v.y < 0)
			{
				if (!PhysicsUtilities.Raycast(Scene.World, controllingBody.Position.ToVec3(),
					-vec3.UnitY * (radius + 0.01f), out var results))
				{
					return true;
				}

				if (-v.y <= golfballData.LandingThreshold)
				{
					OnLanding(triangle, p, normal);

					return false;
				}

				// TODO: Consider giving an upward boost based on spin (similar to hitting a wall while grounded).
				var pos = controllingBody.Position;
				pos.Y = triangle.Average(t => t.y) + radius;
				controllingBody.Position = pos;
				v = Utilities.Reflect(v, normal) * bounciness;
			}

			controllingBody.LinearVelocity = v.ToJVector();

			return false;
		}

		private void OnLanding(vec3[] triangle, vec3 p, vec3 normal)
		{
			isAirborne = false;
			ground = new SurfaceTriangle(triangle, normal, 0);

			var v = controllingBody.LinearVelocity.ToVec3();
			var projected = Utilities.ProjectOntoPlane(v, normal);

			controllingBody.LinearVelocity = projected.ToJVector();
			controllingBody.IsAffectedByGravity = false;
			controllingBody.Position = (p + normal * radius).ToJVector();
		}
		*/

		private void BecomeAirborne()
		{
			flags |= GolfballFlags.IsAirborne;
			flags &= ~GolfballFlags.IsStopped;
		}

		/*
		private void PostStep()
		{
			const float Epsilon = 0.001f;
			
			if (isAirborne)
			{
				return;
			}

			var body = ControllingBody;
			var p = body.Position.ToVec3();

			// If the projection returns true, that means the ball is still within the current triangle.
			if (ground.Project(p, out vec3 result))
			{
				// This helps prevent very, very slow drift while supposedly stationary (on the order of 0.0001 per
				// second).
				if (Utilities.DistanceSquared(p, result) > Epsilon)
				{
					body.Position = (result + new vec3(0, radius, 0)).ToJVector();
				}

				return;
			}

			// TODO: Store a better reference to static meshes (rather than querying the world every frame).
			var world = Scene.World;
			var map = world.RigidBodies.First(b => b.Shape is TriangleMeshShape);
			var n = ground.Normal;
			var range = radius + 0.1f;

			SurfaceTriangle newGround = null;

			// TODO: Does the result normal need to be checked? (to ensure it points up)
			// This means that the golf ball moved to a new triangle. Different actions are taken based on the new
			// slope.
			if (PhysicsUtilities.Raycast(world, map, p, -n, range, out var results) && results.Triangle != null)
			{
				var rN = results.Normal;
				var d1 = Utilities.Dot(controllingBody.LinearVelocity.ToVec3(), rN);
				var d2 = 1 - Utilities.Dot(ground.Normal, rN);

				// This means the new triangle is steeper (in an upward direction).
				if (d1 < 0)
				{
					// This means the new slope is shallow enough (relative to the current slope) to maintain ground
					// control.
					if (d2 < golfballData.TransitionThreshold)
					{
						newGround = new SurfaceTriangle(results.Triangle, rN, 0);
					}
					// TODO: Reflect the ball up.
					else
					{
						return;
					}
				}
				// This means the new triangle is sloped down (relative to the current triangle), but shallow enough to
				// maintain ground control.
				else if (d2 < golfballData.LedgeThreshold)
				{
					newGround = new SurfaceTriangle(results.Triangle, rN, 0);
				}
			}

			if (newGround != null)
			{
				// TODO: Modify velocity based on the new triangle.
				return;
			}

			// This means the ball has rolled off a ledge (either by rolling into open air or above a new triangle
			// that's downward enough to temporarily gain air).
			if (SurfaceTriangle.ComputeForgiveness(controllingBody.Position.ToVec3(), ground) >
				golfballData.EdgeForgiveness)
			{
				BecomeAirborne();
			}
		}
		*/

		public void JellyPrecompute()
		{
			// TODO: Handle edges and points as well.
			for (int i = 0; i < jellySurfaces.Count; i++)
			{
				var s0 = jellySurfaces[i];
				var s1 = jellySurfaces[i + 1];
				var s2 = jellySurfaces[i + 2];
				var b = Utilities.ComputeBarycentric(position, s0.Position, s1.Position, s2.Position);
				var m = s0.Mass * b.x + s1.Mass * b.y + s2.Mass * b.z;

				// Impacts are scaled based on relative mass.
				var dV = linearVelocity * Game.DeltaTime * (mass / (m + mass));

				s0.Position += dV * b.x;
				s1.Position += dV * b.y;
				s2.Position += dV * b.z;
			}
		}

		public override void Update()
		{
			if (JellyMesh == null)
			{
				UpdateRigid();
			}
			else
			{
				UpdateJelly();
			}

			SetPosition(position, true);
			SetOrientation(orientation, true);

			base.Update();

			if (player.IsLocal)
			{
				Scene.Peer.Send(new BallPacket(player.Id, position, linearVelocity, angularVelocity, orientation),
					NetDeliveryMethod.UnreliableSequenced);

				var list = Scene.Canvas.GetElement<DebugView>().GetGroup("Golfball");
				list.Add("Flags: " + flags);
				list.Add($"Position: {position.x:F3}, {position.y:F3}, {position.z:F3}");
				list.Add($"Linear: {linearVelocity.x:F3}, {linearVelocity.y:F3}, {linearVelocity.z:F3}");
				list.Add($"Angular: {angularVelocity.x:F3}, {angularVelocity.y:F3}, {angularVelocity.z:F3}");
				list.Add("Surfaces: " + surfaces.Count);
				list.Add("Edges: " + edges.Count);
				list.Add("Points: " + points.Count);
				list.Add("Should update: " + shouldUpdate);
			}
		}

		private void UpdateRigid()
		{
			if (!shouldUpdate)
			{
				return;
			}

			// TODO: Ensure gravity is applied in all appropriate spots (such as losing speed halfway through a loop).
			if (IsAirborne)
			{
				linearVelocity += gravity * Game.DeltaTime;

				if (!Sweep())
				{
					position += linearVelocity * Game.DeltaTime;
				}
			}
			else
			{
				UpdateRigidGround();
			}

			ApplyAngularVelocity();
			SetPosition(position, true);
			SetOrientation(orientation, true);

			/*
			var dV = linearVelocity * Game.DeltaTime;
			var points = octree.Positions.Select(p => p.ToVec3()).ToArray();
			var tris = octree.Triangles;
			var tMin = float.MaxValue;
			var pResult = vec3.Zero;
			var nResult = vec3.Zero;
			var type = SweptCollisionTypes.None;

			vec3[] triangle = null;

			foreach (var tri in tris)
			{
				var vertices = new[]
				{
					points[tri.I0],
					points[tri.I1],
					points[tri.I2]
				};

				var n = Utilities.ComputeNormal(vertices, WindingTypes.CounterClockwise);

				// While grounded, triangles that are coplanar (or very close to coplanar) are ignored.
				if (isGrounded)
				{
					var d = Utilities.Dot(linearVelocity, n);

					// TODO: Probably compare resulting bounce to gravity to determine whether to ignore the triangle?
					// This first dot check is meant to reject false positives against steeper-down triangles (those
					// are handled via ground raycasts instead).
					if (d > 0 || 1 - Math.Abs(Utilities.Dot(ground.Normal, n)) <= 0.001f)
					{
						continue;
					}
				}

				// TODO: Precompute and store these objects on the mesh itself.
				var precomputed = new PrecomputedTriangle(vertices, n);

				// TODO: Handle embedded spheres.
				// TODO: Handle hitting multiple triangles at exactly the same time.
				// TODO: Handle surface material.
				if (PhysicsHelper.SweepSphere(position, dV, radius, precomputed, vec3.Zero, out var t, out var result,
					out var resultType) && t >= 0 && t < tMin)
				{
					tMin = t;
					pResult = result;
					nResult = precomputed.Normal;
					type = resultType;
					triangle = precomputed.Vertices;
				}
			}

			// This means that a triangle collision was found (using a spherical sweep).
			if (tMin <= 1)
			{
				OnCollision(triangle, pResult, nResult, type);
			}
			else
			{
				position += dV;

				if (isGrounded)
				{
					// TODO: Use surface friction (rather than hardcoding slowdown).
					linearVelocity = Slowdown(linearVelocity, 1, out var stopped);

					// TODO: Probably consider angular velocity here as well.
					if (stopped)
					{
						OnStop();
					}
				}

				// Angular velocity is applied while airborne as well.
				ApplyAngularVelocity();
			}

			if (!isGrounded)
			{
				CastGround();

				// TODO: Check friction (in order to be able to more reliably stop, especially on slopes that aren't perfectly flat).
				// Golf balls roll down slanted slopes (based on surface friction).
				if (isGrounded)
				{
					var n = ground.Normal;
					var cross = Utilities.Cross(n, new vec3(n.x, 0, n.z));
					var q = quat.FromAxisAngle(Constants.PiOverTwo, cross);
					var g = 1 - Utilities.Dot(n, vec3.UnitY);

					linearVelocity += q * n * g;

					// TODO: Account for friction when applying angular velocity.
					// While grounded, angular velocity is artificially set to match flat movement.
					angularVelocity = new vec3(linearVelocity.z, 0, -linearVelocity.x) * Constants.Pi;
				}
			}
			// This implements a temporary kill plane.
			else if (position.y < -3)
			{
				ResetToCheckpoint();
			}
			*/
		}

		private bool Sweep()
		{
			var dV = linearVelocity * Game.DeltaTime;

			if (!PhysicsHelper.SweepSphere(position, dV, radius, courseMesh, out var t, out var results))
			{
				return false;
			}

			position += dV * t;

			var nList = results.Select(r => r.ComputeNormal(position)).ToList();
			var nAverage = Utilities.Normalize(nList.Aggregate(vec3.Zero, (current, n) => current + n) / nList.Count);
			var u = Utilities.Reflect(linearVelocity, nAverage);
			var direct = Utilities.Project(linearVelocity, nAverage);

			linearVelocity = u + direct * (1 - bounciness);

			var isLanded = false;

			for (int i = 0; i < results.Count; i++)
			{
				var n = nList[i];
				var projected = Utilities.Project(linearVelocity, n);
				var g = Utilities.Project(gravity, n) * Game.DeltaTime;

				if (Utilities.LengthSquared(projected) <= Utilities.LengthSquared(g))
				{
					var result = results[i];

					switch (result.Type)
					{
						case SweptCollisionTypes.Surface:
							// TODO: Pull triangle index (from the sweep raycast).
							surfaces.Add(new SurfaceTriangle(result.Triangle.Points, n, 0, -1));

							break;

						case SweptCollisionTypes.Edge:
							edges.Add(result.Edge);

							break;

						case SweptCollisionTypes.Point:
							points.Add(result.Point);

							break;
					}
					
					isLanded = true;
				}
			}

			if (isLanded)
			{
				flags &= ~GolfballFlags.IsAirborne;
				linearVelocity = Utilities.ProjectOntoPlane(linearVelocity, nAverage);
			}

			return true;
		}

		private void UpdateRigidGround()
		{
			var pList = new List<vec3>();
			var nList = new List<vec3>();

			// TODO: Handle points.
			// TODO: Sweep for hits while grounded.
			if (edges.Count > 0)
			{
				var crossList = new List<vec3>();
				var resolutionList = new List<vec3>();
				var v = linearVelocity + gravity * Game.DeltaTime;
				var p = position + v * Game.DeltaTime;

				for (int i = edges.Count - 1; i >= 0; i--)
				{
					var edge = edges[i];
					var e = Utilities.ClosestPointOnLine(p, edge.P1, edge.P2);
					var d = Utilities.DistanceSquared(p, e);

					// This means that the ball has separated from the edge.
					if (d > radius * radius + Constants.DefaultEpsilon)
					{
						edges.RemoveAt(i);
					}
					else
					{
						d = (float)Math.Sqrt(d);

						var n = (p - e) / d;
						var cross = Utilities.Normalize(Utilities.ProjectOntoPlane(n, gravity));

						nList.Add(n);
						crossList.Add(cross);
						resolutionList.Add(n * (radius - d));
					}
				}

				var count = nList.Count;

				if (count > 0)
				{
					var nResult = vec3.Zero;
					var cross = vec3.Zero;

					for (int i = 0; i < count; i++)
					{
						nResult += nList[i];
						cross += crossList[i];
					}

					nResult = Utilities.Normalize(nResult / count);
					cross /= count;

					// TODO: Account for the cross being zero (meaning that the ball must be between two or more balanced edges).
					position = p + Utilities.ProjectedAverage(resolutionList);
					linearVelocity += Utilities.Project(nResult, cross);
				}
				else if (!IsAirborne)
				{
					position = p;
					BecomeAirborne();
				}

				return;
			}

			// ===== SURFACES =====

			pList.Clear();
			nList.Clear();

			var nAverage = Utilities.Average(surfaces.Select(s => s.Normal).ToList(), true);

			linearVelocity += Utilities.ProjectOntoPlane(gravity, nAverage) * Game.DeltaTime;
			position += linearVelocity * Game.DeltaTime;

			// Current surfaces are checked first (to see whether the ball has moved off of any).
			for (int i = surfaces.Count - 1; i >= 0; i--)
			{
				var surface = surfaces[i];
				var n = surface.Normal;

				// TODO: Does drift need to be checked here? (for slightly slanted surfaces with enough friction to cause a full stop)
				if (surface.Project(position, out var point))
				{
					pList.Add(point);
					nList.Add(n);

					continue;
				}

				surfaces.RemoveAt(i);

				if (courseMesh.Raycast(position, -surface.Normal, radius + 0.1f, out var results))
				{
					var rN = results.Normal;
					var d1 = Utilities.Dot(linearVelocity, rN);

					// This means the new triangle is steeper (in an upward direction) relative to the current surface.
					// This should never happen (since the sweep should catch the collision first).
					if (d1 < -Constants.DefaultEpsilon)
					{
						Debug.Fail("Steeper (upward) triangle hit via surface raycast. This should never happen. " +
							"Should be investigated (although the solution might be to simple ignore the raycast " +
							"result).");
					}

					var d2 = Utilities.Dot(n, rN);
					var rP = results.Position;

					// TODO: Use gravity (and the projected new normal) to determine smooth transitions (rather than comparing distance to an arbitrary epsilon).
					// If the new triangle is nearly identical in slope, the surface can transition smoothly. The new
					// surface must also be aligned with the ball (at the point of transition).
					if (1 - d2 <= Constants.DefaultEpsilon &&
						Math.Abs(radius * radius - Utilities.DistanceSquared(position, rP)) < Constants.DefaultEpsilon)
					{
						pList.Add(rP);
						nList.Add(rN);
						surfaces.Add(new SurfaceTriangle(results.Triangle, rN, results.Material, -1));
					}
				}
				else
				{
					// When the ball falls off a surface, if the linear velocity is slow enough (relative to gravity)
					// that the ball would hit the nearest edge/point on the next frame, control is immediately
					// transferred (instead of becoming airborne for a frame).
					var v = linearVelocity + gravity * Game.DeltaTime;
					var dV = v * Game.DeltaTime;

					// TODO: Probably store the precomputed triangle on the surface itself (since it's needed for sweeps anyway).
					var precomputed = new PrecomputedTriangle(surface.Points, n);

					// TODO: Verify the resulting normal against the normals of any other contacts (to make sure the snap doesn't embed the ball).
					if (PhysicsHelper.SweepSphere(position, dV, radius, precomputed, vec3.Zero, out var t,
						out var result))
					{
						// It's possible (though unlikely) that this short sweep will hit another object first. If that
						// happens, the result can be ignored here (since it'll be caught by the next sweep anyway, I
						// think).
						switch (result.Type)
						{
							case SweptCollisionTypes.Edge:
								if (surface.HasEdge(result.Edge))
								{
									edges.Add(result.Edge);
								}

								break;

							case SweptCollisionTypes.Point:
								break;
						}
					}
				}

				/*
				else if (surface.ComputeForgiveness(p) > golfballData.EdgeForgiveness)
				{
					surfaces.RemoveAt(i);
				}
				*/
			}

			if (pList.Count > 0)
			{
				position = pList[0] + nList[0] * radius;
				angularVelocity = new vec3(linearVelocity.z, 0, -linearVelocity.x) * Constants.Pi;
			}
			// This means that the ball has separated from all contacts (meaning it should become airborne).
			else
			{
				BecomeAirborne();
			}
		}

		/*
		private bool CastGround(SurfaceTriangle surface, vec3 p)
		{
			const float Epsilon = 0.001f;

			// This means that the ball is still within the current triangle.
			if (ground.Project(position, out vec3 result))
			{
				var p = result + groundQuat * new vec3(0, radius, 0);

				// This helps prevent very, very slow drift while supposedly stationary (on the order of 0.0001 per
				// second).
				if (Utilities.DistanceSquared(position, p) > Epsilon)
				{
					position = p;
				}

				return;
			}

			if (courseMesh.Raycast(position, -n, range, out var results))
			{
				var rN = results.Normal;
				var d1 = Utilities.Dot(linearVelocity, rN);

				// This means the new triangle is steeper (in an upward direction). At this point in the code, this
				// likely indicates a false positive (since the sweep test above should catch any upward hits).
				if (d1 < 0)
				{
					return;
				}

				var d2 = Utilities.Dot(n, rN);
				var rP = results.Position;

				// If the new triangle is nearly identical in slope, the ground can transition smoothly (without
				// gaining air). The new surface must also be aligned with the bottom of the ball.
				if (1 - d2 <= Epsilon &&
					Math.Abs(radius * radius - Utilities.DistanceSquared(position, rP)) < Epsilon)
				{
					position = rP + rN * radius;
					linearVelocity = Utilities.ProjectOntoPlane(linearVelocity, n);

					RefreshGround(results.Triangle, n, 0);

					return;
				}

				// TODO: Is this actually possible, or will it always be caught by the sweep? Probably safer to keep it anyway?
				// This means the new triangle is steeper (in an upward direction).
				if (d < 0)
				{
					// This means the new slope is shallow enough (relative to the current slope) to maintain ground
					// control.
					if (d < golfballData.TransitionThreshold)
					{
						newGround = new SurfaceTriangle(results.Triangle, rN, 0);
					}
					// TODO: Reflect the ball up.
					else
					{
						return;
					}
				}
				// This means the new triangle is sloped down (relative to the current triangle). If the ball is moving
				// slow enough that the next gravity tick would touch the triangle again, ground control is immediately
				// transferred instead.
				// TODO: (for next time) use the slope of the new triangle in this calculation
				else if (Utilities.LengthSquared(linearVelocity.swizzle.xz) <= PhysicsConstants.Gravity)
				{
					newGround = new SurfaceTriangle(results.Triangle, rN, 0);
				}
			}
		}
		*/
		
		private void UpdateJelly()
		{
			if ((flags & GolfballFlags.IsAirborne) > 0)
			{
				if (JellyMesh.Sweep(this, out var t))
				{
					position += linearVelocity * t;
					flags &= ~GolfballFlags.IsAirborne;
				}
				else
				{
					linearVelocity.y -= PhysicsConstants.Gravity * Game.DeltaTime;
					position += linearVelocity * Game.DeltaTime;
				}

				return;
			}

			var pushback = new List<vec3>();
			
			// Resolve surfaces.
			for (int i = 0; i < jellySurfaces.Count; i += 3)
			{
				var s0 = jellySurfaces[i];
				var s1 = jellySurfaces[i + 1];
				var s2 = jellySurfaces[i + 2];
				var b = Utilities.ComputeBarycentric(position, s0.Position, s1.Position, s2.Position);
				var m = s0.Mass * b.x + s1.Mass * b.y + s2.Mass * b.z;

				// Ball just hit a triangle (and applied an angled force downward to the triangle)
				// On the next frame, the springs move downward (due to that force), but are also partially pushed back by spring forces
				// That pushback is summed (via projection) and recorded (i.e. this triangle is pushing back by this amount in this direction)
				// That sum is used to modify the ball's linear velocity
				// If the ball's velocity is still moving relatively into the surface (based on normal and velocity at contact point), the ball carries the surface (again weighted by barycentric)
				// At some point, that balance will flip and the pushback (i.e. surface point velocity) will overpower the ball's velocity
				// At that point, the triangle starts carrying the ball (i.e. ball moves with projected velocity, projects onto the point, and has its velocity updated)
				// At another point, the velocity delta of the surface points will be negative relative to ball velocity (at which point the ball separates from the surface)
			}
		}

		private void OnStop()
		{
			flags |= GolfballFlags.IsStopped;
			flags &= ~GolfballFlags.IsRespawnAvailable;
			checkpoint.Refresh();
		}

		/*
		private void OnCollision(vec3[] triangle, vec3 p, vec3 n, SweptCollisionTypes type)
		{
			var reflection = vec3.Zero;

			if (type == SweptCollisionTypes.Embedded)
			{
				reflection = position - p;
				p = Utilities.Normalize(reflection) * radius;
			}

			position = p;

			// This values are used for both surface hits and grounded wall hits. Little awkward to code, but I didn't
			// want to repeat the calculations.
			/*
			var u1 = vec3.Zero;
			var direct = vec3.Zero;

			if (!isAirborne || type == SweptCollisionTypes.Surface)
			{
				u1 = Utilities.ProjectOntoPlane(linearVelocity, n);
				direct = linearVelocity - u1;
			}

			var isAirborne = !IsAirborne;

			if (isAirborne)
			{
				switch (type)
				{
					case SweptCollisionTypes.Edge:
						var d = float.MaxValue;
						var linePoint = vec3.Zero;

						for (int i = 0; i < 3; i++)
						{
							var p1 = triangle[i];
							var p2 = triangle[(i + 1) % 3];
							var distance = Utilities.DistanceSquaredToLine(p, p1, p2, out var t);

							if (distance < d)
							{
								d = distance;
								linePoint = p1 + (p2 - p1) * t;
							}
						}

						// When a golf ball hits an edge, part of the impact goes towards reflecting linear velocity
						// (i.e. bouncing) and the rest goes towards angular velocity.
						/*
						var u = Utilities.Normalize(p - linePoint);
						var v = Utilities.Reflect(-linearVelocity, u);
						
						linearVelocity = v * bounciness;

						reflection = Utilities.Normalize(p - linePoint);

						break;

					case SweptCollisionTypes.Point:
						break;

					case SweptCollisionTypes.Surface:
						reflection = n;

						break;
				}
			}
			else
			{
				reflection = n;

				//linearVelocity = Utilities.Reflect(linearVelocity, n);
				//linearVelocity += direct * (1 - bounciness);

				// TODO: Restore upward boosting on grounded collisions.
				/*
				// When a grounded golf ball collides with another triangle steep enough to not transition, the ball is
				// given a small vertical boost based on spin (i.e. a faster spin in the direction of a wall gives a
				// stronger boost). This boost is further modified (i.e. reduced) using a dot product, such that
				// head-on collisions receive the strongest boost, while glancing blows barely get anything.
				var vFlat = linearVelocity.swizzle.xz;
				var flatLength = Utilities.Length(vFlat);

				// TODO: Consider squaring the dot product (i.e. apply exponentially, not linearly).
				var dot = Utilities.Dot(-vFlat / flatLength, Utilities.Normalize(n.swizzle.xz));

				// TODO: This should really be based on angular velocity rather than linear.
				// The divisor here is arbitrary.
				var correction = Math.Min(flatLength / 60 * dot, golfballData.MaxBoost);
				var nCorrected = Utilities.Normalize(n + new vec3(0, correction, 0));

				linearVelocity = Utilities.Reflect(linearVelocity, nCorrected) * bounciness;

				// When a bounce occurs, angular velocity is artificially set to rotate in the new direction of
				// movement (relative to the flat XZ plane). This is something I noticed was pretty visually satisfying
				// when I created a prototype physics-based game for Moon Studios.
				angularVelocity = new vec3(linearVelocity.z, 0, -linearVelocity.x) * Constants.Pi;

				if (linearVelocity.y > PhysicsConstants.Gravity)
				{
					BecomeAirborne();
				}
			}

			var u = Utilities.Reflect(linearVelocity, reflection);
			var direct = Utilities.Project(linearVelocity, reflection);
				
			linearVelocity = u + direct * (1 - bounciness);
			
			if (isAirborne && type == SweptCollisionTypes.Surface)
			{
				direct = Utilities.Project(linearVelocity, reflection);

				if (Utilities.Length(direct) <= PhysicsConstants.Gravity * Game.DeltaTime)
				{
					// TODO: Compute the new IsStopped flag immediately (rather than waiting until the next frame).
					flags &= ~GolfballFlags.IsAirborne;
					linearVelocity = Utilities.ProjectOntoPlane(linearVelocity, reflection);
					RefreshGround(triangle, n, 0);
				}
			}
		}
		*/

		private void ApplyAngularVelocity()
		{
			const float Dt = Game.DeltaTime;
			
			eulerOrientation += angularVelocity * Game.DeltaTime;

			// This code is taken from the Jitter World class (based on how angular velocity is applied to physics
			// bodies).
			var angle = Utilities.Length(angularVelocity);

			vec3 axis;

			if (angle < 0.001f)
			{
				axis = angularVelocity * (0.5f * Dt - Dt * Dt * Dt * 0.020833333333f * angle * angle);
			}
			else
			{
				axis = angularVelocity * (float)Math.Sin(0.5f * angle * Dt) / angle;
			}

			var q = new quat(axis.x, axis.y, axis.z, (float)Math.Cos(angle * Dt * 0.5f));

			orientation = (q * orientation).Normalized;
			angularVelocity = Slowdown(angularVelocity, golfballData.AerialAngularDeceleration, out _);
		}

		private vec3 Slowdown(vec3 v, float deceleration, out bool stopped)
		{
			const float Epsilon = 0.0001f;

			int ComputeSign(vec3 u)
			{
				return Math.Sign(u.x != 0 ? u.x : (u.y != 0 ? u.y : u.z));
			}

			if (Utilities.LengthSquared(v) < Epsilon)
			{
				stopped = true;

				return vec3.Zero;
			}

			var oldSign = ComputeSign(v);

			v -= Utilities.Normalize(v) * deceleration * Game.DeltaTime;

			var newSign = ComputeSign(v);

			if (oldSign != newSign)
			{
				stopped = true;
				v = vec3.Zero;
			}
			else
			{
				stopped = false;
			}

			return v;
		}

		/*
		private void CastGround()
		{
			const float Epsilon = 0.001f;

			// This means that the ball is still within the current triangle.
			if (ground.Project(position, out vec3 result))
			{
				var p = result + groundQuat * new vec3(0, radius, 0);

				// This helps prevent very, very slow drift while supposedly stationary (on the order of 0.0001 per
				// second).
				if (Utilities.DistanceSquared(position, p) > Epsilon)
				{
					position = p;
				}

				return;
			}

		    // TODO: Store a better reference to static meshes (rather than querying the world every frame).
		    var world = Scene.World;
		    var map = world.RigidBodies.First(b => b.Shape is TriangleMeshShape);

			// The range here is pretty arbitrary. The only really important thing is that it's larger than the radius.
		    var range = radius + 0.1f;
			var n = ground.Normal;

			// TODO: Does the result normal need to be checked? (to ensure it points up)
			// This means that the golf ball moved to a new triangle. Different actions are taken based on the new
			// slope.
			//if (PhysicsUtilities.TryRaycast(world, map, position, -n, range, out var results))

			if (courseMesh.Raycast(position, -n, range, out var results))
		    {
		        var rN = results.Normal;
		        var d1 = Utilities.Dot(linearVelocity, rN);

			    // This means the new triangle is steeper (in an upward direction). At this point in the code, this
				// likely indicates a false positive (since the sweep test above should catch any upward hits).
				if (d1 < 0)
			    {
				    return;
			    }

			    var d2 = Utilities.Dot(n, rN);
				var rP = results.Position;

				// If the new triangle is nearly identical in slope, the ground can transition smoothly (without
				// gaining air). The new surface must also be aligned with the bottom of the ball.
				if (1 - d2 <= Epsilon &&
					Math.Abs(radius * radius - Utilities.DistanceSquared(position, rP)) < Epsilon)
				{
					position = rP + rN * radius;
					linearVelocity = Utilities.ProjectOntoPlane(linearVelocity, n);

					RefreshGround(results.Triangle, n, 0);

					return;
				}

			    // TODO: Is this actually possible, or will it always be caught by the sweep? Probably safer to keep it anyway?
			    // This means the new triangle is steeper (in an upward direction).
	            if (d < 0)
	            {
	                // This means the new slope is shallow enough (relative to the current slope) to maintain ground
	                // control.
	                if (d < golfballData.TransitionThreshold)
	                {
	                    newGround = new SurfaceTriangle(results.Triangle, rN, 0);
	                }
	                // TODO: Reflect the ball up.
	                else
	                {
	                    return;
	                }
	            }
	            // This means the new triangle is sloped down (relative to the current triangle). If the ball is moving
			    // slow enough that the next gravity tick would touch the triangle again, ground control is immediately
			    // transferred instead.
			    // TODO: (for next time) use the slope of the new triangle in this calculation
	            else if (Utilities.LengthSquared(linearVelocity.swizzle.xz) <= PhysicsConstants.Gravity)
	            {
	                newGround = new SurfaceTriangle(results.Triangle, rN, 0);
	            }
		    }

			/*
		    if (newGround != null)
		    {
			    // TODO: Modify velocity based on the new triangle.
                RefreshGround(newGround);

		        return;
		    }

			// This means the ball has rolled off a ledge (either by rolling into open air or above a new triangle
			// that's steep enough to gain air).
			if (SurfaceTriangle.ComputeForgiveness(position, ground) > golfballData.EdgeForgiveness)
		    {
		        BecomeAirborne();
		    }
        }
		*/

		/*
		private void RefreshGround(vec3[] triangle, vec3 n, int material)
	    {
		    ground = new SurfaceTriangle(triangle, n, material);
		    groundQuat = Utilities.Orientation(vec3.UnitY, n);

			//RefreshGround(new SurfaceTriangle(triangle, n, material));
		}
		*/
		
		/*
        private void RefreshGround(SurfaceTriangle newGround)
        {
			ground = newGround;
			groundQuat = Utilities.Orientation(vec3.UnitY, newGround.Normal);
	    }
		*/

		private class Checkpoint
		{
			private Golfball golfball;

			public Checkpoint(Golfball golfball)
			{
				this.golfball = golfball;
			}

			// TODO: Track surfaces (it's possible to stop on multiple at once).
			public vec3 Position { get; private set; }
			public quat Orientation { get; private set; }

			public void Refresh()
			{
				Position = golfball.position;
				Orientation = golfball.orientation;
			}
		}
	}
}
