﻿using Lidgren.Network;
using Minigolf.Players;

namespace Minigolf
{
	public static class MinigolfExtensions
	{
		public static PlayerId ReadPlayerId(this NetIncomingMessage message)
		{
			var name = message.ReadString();
			var tag = message.ReadInt16();

			return new PlayerId(name, tag);
		}

		public static void Write(this NetOutgoingMessage message, PlayerId id)
		{
			message.Write(id.Name);
			message.Write((short)id.Tag);
		}
	}
}
