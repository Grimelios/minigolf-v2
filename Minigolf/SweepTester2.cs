﻿using System.Linq;
using Engine;
using Engine.Core;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Physics;
using Engine.Shapes._3D;
using Engine.Utility;
using GlmSharp;
using Minigolf.Physics;
using static Engine.GLFW;

namespace Minigolf
{
	public class SweepTester2
	{
		private PrimitiveRenderer3D primitives;
		private SpriteText text1;
		private SpriteText text2;
		private SpriteText text3;

		private vec3 position;
		private vec3 target;
		private vec3[] triangle;

		public SweepTester2(PrimitiveRenderer3D primitives)
		{
			this.primitives = primitives;

			triangle = new []
			{
				new vec3(1, 0, 0),
				new vec3(-1, 0, 1),
				new vec3(-1, 0, -1)
			};

			position = new vec3(2, 0.25f, 0);
			target = vec3.Zero;

			var font = ContentCache.GetFont("Debug");
			text1 = new SpriteText(font);
			text1.Position.SetValue(new vec2(10), false);
			text2 = new SpriteText(font);
			text2.Position.SetValue(new vec2(10, 26), false);
			text3 = new SpriteText(font);
			text3.Position.SetValue(new vec2(10, 42), false);

			InputProcessor.Add(data =>
			{
				const float Speed = Game.DeltaTime * 3;

				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				bool x = keyboard.Query(GLFW_KEY_X, InputStates.Held);
				bool y = keyboard.Query(GLFW_KEY_Y, InputStates.Held);
				bool z = keyboard.Query(GLFW_KEY_Z, InputStates.Held);
				bool v = keyboard.Query(GLFW_KEY_LEFT_SHIFT, InputStates.Held);
				bool up = keyboard.Query(GLFW_KEY_UP, InputStates.Held);
				bool down = keyboard.Query(GLFW_KEY_DOWN, InputStates.Held);

				if (x && up ^ down)
				{
					if (v)
					{
						target.x += (up ? 1 : -1) * Speed;
					}
					else
					{
						position.x += (up ? 1 : -1) * Speed;
					}
				}

				if (y && up ^ down)
				{
					if (v)
					{
						target.y += (up ? 1 : -1) * Speed;
					}
					else
					{
						position.y += (up ? 1 : -1) * Speed;
					}
				}

				if (z && up ^ down)
				{
					if (v)
					{
						target.z += (up ? 1 : -1) * Speed;
					}
					else
					{
						position.z += (up ? 1 : -1) * Speed;
					}
				}
			});
		}

		public SpriteText[] Texts => new []
		{
			text1, 
			text2,
			text3
		};

		public void Draw()
		{
			const int Segments = 32;

			const float Length = 0.2f;
			const float Radius = 0.3f;
			const float Increment = Constants.TwoPi / Segments;

			var precomputed = new PrecomputedTriangle(triangle, WindingTypes.CounterClockwise);
			var center = triangle.Aggregate(vec3.Zero, (current, v) => current + v) / 3;

			primitives.DrawTriangle(triangle, Color.Purple);
			primitives.DrawLine(center, center + precomputed.Normal, Color.Blue);

			text1.Value = "Type:";
			text2.Value = "T:";
			text3.Value = "Result:";

			if (position != target)
			{
				// Draw spheres.
				var color = new Color(50);
				var q = Utilities.LookAt(position, target);
				var sphere1 = new Sphere(Radius);
				sphere1.Position = position;
				sphere1.Orientation = q;

				var sphere2 = new Sphere(Radius);
				sphere2.Position = target;
				sphere2.Orientation = q;

				primitives.Draw(sphere1, color, Segments);
				primitives.Draw(sphere2, color, Segments);

				// Draw lines.
				for (int i = 0; i < Segments; i++)
				{
					var d = Utilities.Direction(Increment * i) * Radius;
					var p = q * new vec3(d.x, d.y, 0);

					primitives.DrawLine(position + p, target + p, color);
				}

				// Draw axes.
				primitives.DrawLine(position + vec3.UnitX * Length, position - vec3.UnitX * Length, Color.Red);
				primitives.DrawLine(position + vec3.UnitY * Length, position - vec3.UnitY * Length, Color.Green);
				primitives.DrawLine(position + vec3.UnitZ * Length, position - vec3.UnitZ * Length, Color.Cyan);

				primitives.DrawLine(target + vec3.UnitX * Length, target - vec3.UnitX * Length, Color.Orange);
				primitives.DrawLine(target + vec3.UnitY * Length, target - vec3.UnitY * Length, Color.Orange);
				primitives.DrawLine(target + vec3.UnitZ * Length, target - vec3.UnitZ * Length, Color.Orange);

				// Draw intersection.
				var success = PhysicsHelper.SweepSphere(position, target - position, Radius, precomputed,
					vec3.Zero, out var tResult, out var resultPoint, out var type);

				text1.Value += " " + type;
				text2.Value += " " + tResult;

				if (success)
				{
					var resultSphere = new Sphere(Radius);
					resultSphere.Position = resultPoint;
					resultSphere.Orientation = q;

					primitives.DrawLine(position, resultPoint, Color.Gold);
					primitives.Draw(resultSphere, Color.White, Segments);

					text3.Value += $" {resultPoint.x:F3}, {resultPoint.y:F3}, {resultPoint.z:F3}";
				}
			}
		}
	}
}
